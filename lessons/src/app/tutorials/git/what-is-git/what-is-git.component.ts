import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-what-is-git',
  templateUrl: './what-is-git.component.html',
  styleUrls: ['./what-is-git.component.css']
})
export class WhatIsGitComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("What is Git?");
  }

  ngOnInit() {
  }

}
