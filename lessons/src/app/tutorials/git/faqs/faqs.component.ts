import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.css']
})
export class FaqsComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("FAQs");
  }

  ngOnInit() {
  }

}
