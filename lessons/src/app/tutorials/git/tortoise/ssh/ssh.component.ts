import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-ssh',
  templateUrl: './ssh.component.html',
  styleUrls: ['./ssh.component.css']
})
export class GitTortoiseSshComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("SSH");
  }

  ngOnInit() {
  }

}
