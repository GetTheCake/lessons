<p>
  Before reading this, I hope that you understand what you are trying to achieve with branching. It is explained in my guide: '<a routerLink="../../what-are-branches">What are 'branches'?</a>'. If you're new to branching, please make sure you've read that first.
</p>

<p>
  Now, assuming you're familiar with branching, I'm going to go ahead and show you how to do the following things with branches in Tortoise Git:
</p>
<ol>
  <li>Create a branch</li>
  <li>Checkout / Switch to a branch</li>
  <li>Push a new branch</li>
  <li>Merge a branch</li>
  <li>Delete a branch</li>
</ol>
<p>
  Let's begin.
</p>

<p style="color:#ff0000;">To keep the text shorter, I will not be instructing you to right-click for bringing up your tortoise menu. I expect you to know how to do this already. I will simply type the shorthand version, such as "Go to <i>ToirtoiseGit > Switch/Checkout</i>". This would mean I want you to right-click at the top-most folder of your git project in windows, and go to your <i>Tortoise Git</i> menu, and select <i>Switch/Checkout.</i></p>

<h2>Create a branch</h2>

<ol>
  <li>Go to TortoiseGit > Create Branch.</li>
  <li>
    <p>
      When the dialogue appears, simply type a name for your new branch, and hit the <i>OK </i>button<i>. </i>Note that professional practice would instruct that your first new branch should be named <i>development, </i>with the expectation that you then make a new branch from <i>development</i> to work on.
    </p>
    <p><img src="assets/images/git/tortoise/branching/createbranchdialogue.png"/></p>
  </li>
</ol>

<h2>Checkout / Switch to a branch</h2>

<p>
  Your branch exists, but we aren't using it yet. Go to <i>TortoiseGit > Switch/Checkout </i>and in the dialogue, select your new branch. This is how you switch branches.
</p>

<p><img src="assets/images/git/tortoise/branching/switchingbranch.jpg"/></p>

<h2>Push a new branch</h2>

<p>
  No, don't push your computer over. That's not what <i>push </i>means when we are talking about git. <i>Push </i>is simply what you do to take changes on your <i>local repository </i>(on your coputer) and<i> push </i>those changes to your <i>remote repository </i>(like gitlab).
</p>

<p>
  Remember, we have created a new branch, and even switched to it. But this branch still only exists on your <i>local repository. Y</i>our team members can't access it, and if you try to work from a different computer, neither can you!
</p>

<p>
  You need to <i>push </i>this new branch to the <i>remote repository. </i>To do that, go to <i>TortoiseGit > Push. </i>When the dialogue appears, simply push the <i>OK </i>button.
</p>

<p><img src="assets/images/git/tortoise/branching/push.png"/></p>

<p>
  Note that this is identical to trying to push when you have committed your work. If you've followed my setup guide correctly, <a routerLink="../getting-started">Getting Started with Tortoise Git for windows</a>, then this should have worked successfully, with blue text, NOT RED:
</p>

<p><img src="assets/images/git/tortoise/branching/successfulpush.png"/></p>

<p>
  If you've used GitLab as your server, like I suggested in <a routerLink="../../getting-started">Getting Started</a>, then for your own peace of mind, you can also confirm the existence of your new branch by logging in to your GitLab account, clicking on your project, and viewing your branches. I won't tell you how to do this, because I bet you can figure it out. But here's what mine looks like on GitLab:
</p>

<p>
  <img src="assets/images/git/tortoise/branching/gitlabbranches.png"/>
</p>

<p>
  Not a necessary step, but this way you know for sure that the branch you created is accessible from places outside of your own computer.
</p>

<p>
  So, now that you have a new branch, you can do THIS magic:
</p>

<p><img src="assets/images/git/tortoise/branching/branching.gif"/></p>

<p>
  Did that not seem magical? Let me explain.
</p>

<p>
  Some files have been created on the development branch, committed and then pushed. In the above GIF, you can see what my project folder looks like when I switch branches. Only the <i>development </i>branch has the 3 extra text files, but when we switch to the <i>master </i>branch, we can see that those files do not exist on <i>master. </i>
</p>

<p>
  Hopefully, the above GIF demonstrates how branches are copies of your project, but with slight differences. So, what if you LIKE the changes on development, and feel like you need these same changes on the <i>master </i>branch? Then, you need to <i>merge </i>your branches!
</p>

<h2>Merge a branch</h2>

<p>
  This process will take <i>differences </i>from another branch, and apply them to whichever branch you are currently working on. To make this magic happen, follow these steps:
</p>

<ol>
  <li>
    Go to <i>TortoiseGit > Switch/Checkout </i>to make sure you are on the branch that wishes to RECEIVE changes. Such as the <i>master</i> branch, as it wishes to receive changes from <i>development. </i>When the dialogue appears, simply push the <i>OK </i>button.
  </li>
  <li>
    <p>
      After you've switched to your new branch, go to <i>TortoiseGit > Merge</i>. When the dialogue shows up, simply select the branch you wish to receive changes from. In this case, I choose <i>development</i>.
    </p>
    <p><img src="assets/images/git/tortoise/branching/merge.jpg"/></p>
    <p>
      Note that the <i>remote</i> versions of these branches are also selectable, this is because you can merge directly from your server, meaning you don't have to <i>Pull</i> changes to your local branch, or even have the branch on your machine in order to merge. You can simply grab the changes directly from the remote repository (AKA the server, AKA GitLab).Once you have successfully merged, you may need to <i>commit</i> and <i>push</i> these changes. As the merge will only have happened on your <i>local repository</i>, not on the <i>remote repository.</i>
    </p>
    <p>
      That's it. You've mastered merging. Grats.
    </p>
  </li>
</ol>

<h2>Deleting a branch</h2>

<p>
  Lastly, you'll want to occasionally delete a branch. This is especially true if you have merged changes from a branch that you don't need to keep anymore. Unfortunately with Tortoise, this is one of the LEAST straightforward features I have found (it is easier in OTHER Git clients, but not in Tortoise). I'm going to be lazy here and just copy someone else's blog post for this (I would love to give credit for this, but as I found this on stackoverflow.com, ALSO un-credited, I have no idea who to credit for this... thanks though).
</p>

<p>
  ...remove the local branch by first opening up the <code>Checkout/Switch</code> dialog to get at the <code>Browse refs</code>dialog.
</p>

<p><img src="assets/images/git/tortoise/branching/De6iG.jpg"/></p>

<p>
  In the <code>Browse refs</code> dialog we can right click on the local branch and choose to delete it.
</p>

<p><img src="assets/images/git/tortoise/branching/IMadz.jpg"/></p>

<p>
  To delete a remote branch we can do the same thing, but instead of right clicking on our local branch we expand the remotes tree in the left part of the dialog and then locate the remote branch.
</p>

<p><img src="assets/images/git/tortoise/branching/JstNW.jpg"/></p>

<p>
  I believe the above steps can also be used to delete branches on your remote, which, by the way, you can also do via some buttons on Gitlab.com.
</p>

<p>
  And that's all there is to know about branching in Tortoise Git! This is also the final topic I intend to cover for now. At this stage, if you have followed and understood my Tortoise Git guides up to this point, great! Also, I'd happily develop some software or games or complex art with you.
</p>

<p>
  Cheers,<br>
  Regan.
</p>