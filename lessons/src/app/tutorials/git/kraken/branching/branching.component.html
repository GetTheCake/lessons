<p>
  Before reading this, I hope that you understand what you are trying to achieve with branching. It is explained in my guide: '<a routerLink="../../what-are-branches/">What are 'branches'?</a>'. If you're new to branching, please make sure you've read that first. Now, assuming you're familiar with branching, I'm going to go ahead and show you how to do the following things with branches in GitKraken:
</p>

<ol>
  <li>Create a branch</li>
  <li>Checkout / Switch to a branch</li>
  <li>Push a new branch</li>
  <li>Merge a branch</li>
  <li>Delete a branch</li>
</ol>

<p>
  This guide strictly describes these procedures when using GitKraken, so hopefully you have come to the right place, and your interface looks something like this:
</p>

<p><img src="assets/images/git/kraken/branching/gitkinterface.png"></p>

<p>
  Let's begin.
</p>

<h2>Create a branch</h2>

<p>
  Branches in Git are like branches in trees, they have to branch off of something, like another branch. Every new project starts with a default branch called '<i>master</i>'. So we will make a branch called <i>development,</i> which will branch off of <i>master</i>.
</p>

<ol>
  <li>Simply right click on your local
    <i>master</i> branch and select  '
    <i>Create branch here</i>'.</li>
  <li>Type the name of your new branch when prompted to do so. I have named mine '
    <i>development</i>'.</li>
</ol>

<p><img src="assets/images/git/kraken/branching/gitkcreatebranch.gif"></p>

<p>
  You will be instantly switched to your new branch.
</p>

<h2>Checkout / Switch to a branch</h2>

<p>
  In some git clients, you must also switch to your new branch after creating it, this happens for you automatically in GitKraken. However, just so you know, you can switch branches whenever you like by double-clicking on them under your local branches section.
</p>

<p>
  <img src="assets/images/git/kraken/branching/gitkbranches.png"/>
</p>

<p>
  Double-clicking on a remote branch that does not exist locally will checkout (AKA switch to) that branch and you will find it in your local branch list from then on.
</p>

<h2>Push a new branch</h2>

<p>
  <i>Push </i>is what you do to take changes on your <i>local repository </i>(on your computer) and <i> push </i>those changes to your <i>remote repository </i>(like Gitlab). Remember, we have created a new branch, and even switched to it. But this branch still only exists on your <i>local repository</i>. Your team members can't access it, and if you try to work from a different computer, neither can you! You need to <i>push </i>this new branch to the <i>remote.</i>
</p>
<ul>
  <li>
    <p>
      Assuming you are currently checked out to your new branch, simply hit the <i>push</i> button at the top of your GitKraken window. Ignore the prompt that appears and hit the <i>submit </i>button.
    </p>
    <p>
      <img src="assets/images/git/kraken/branching/gitkpush.png">
    </p>
  </li>
  <li>
    If you are not currently checked out to the branch you intend to <i>push</i>, then you can just right-click on the <i>local branch</i> you intend to push, and select '<i>push'. </i>Ignore the prompt that appears and hit the submit button.
  </li>
</ul>
<p>
  Note that this is identical to trying to push when you have committed your work. If you've followed my setup guide correctly, <a routerLink="../getting-started">Getting Started with GitKraken for Windows</a>, then this should have worked successfully without any errors If you've used GitLab as your server, like I suggested in <a routerLink="../../getting-started">Getting Started</a>, then for your own peace of mind, you can also confirm the existence of your new branch by logging in to your GitLab account, clicking on your project, and viewing your branches. I won't tell you how to do this, because I bet you can figure it out. But here's what mine looks like on GitLab:
</p>

<p>
  <img src="assets/images/git/kraken/branching/gitlabbranches.png">
</p>

<p>
  Not a necessary step, but this way you know for sure that the branch you created is accessible from places outside of your own computer. So, now that you have a new branch, you can do THIS magic:
</p>

<p>
  <img src="assets/images/git/kraken/branching/gitkswitchbranches.gif">
</p>

<p>
  Did that not seem magical? Let me explain. Some files have been created on the development branch, committed and then pushed. In the above GIF, you can see what my project folder looks like when I switch branches. Only the <i>development </i>branch has the 3 extra text files, but when we switch to the <i>master </i>branch, we can see that those files do not exist on <i>master</i>.
</p>

<p>
  Hopefully, the above GIF demonstrates how branches are copies of your project, but with slight differences. So, what if you LIKE the changes on development, and feel like you need these same changes on the <i>master </i>branch? Then, you need to <i>merge </i>your branches!
</p>

<h2>Merge a branch</h2>

<p>
  This process will take <i>differences </i>from another branch, and apply them to whichever branch you are currently working on. To make this magic happen, simply drag one local branch into another branch, it should open a menu, from which you want to select '<i>Merge branchX into branchY</i>', like so:
</p>

<p><img src="assets/images/git/kraken/branching/gitkmerge.gif"></p>

<p>
  Once you have successfully merged, you may need to <i>commit</i> and <i>push</i> these changes. As the merge will only have happened on your <i>local</i>, not on the <i>remote repository.</i>
</p>

<p>
  That's it. You've mastered merging. Grats.
</p>

<h2>Deleting a branch</h2>

<p>
  This is easy in GitKraken, simply right-click on any branch, either in <i>local</i> or <i>remote</i>, a menu will appear, select '<i>Delete branchX</i>'. Note that you can't delete a branch that you're currently checked out to, you must checkout to a different branch first.
</p>

<p><img src="assets/images/git/kraken/branching/gitkdelete.jpg"></p>

<h2>That's it...</h2>

<p>
  That's all there is to know about branching in GitKraken! This is also the final topic I intend to cover for now. At this stage, if you have followed and understood my GitKraken guides up to this point, great! Also, I'd happily develop some software or games or complex art with you.
</p>

<p>
  Cheers,<br>
  Regan.
</p>