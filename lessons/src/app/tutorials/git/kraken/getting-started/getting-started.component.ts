import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-getting-started',
  templateUrl: './getting-started.component.html',
  styleUrls: ['./getting-started.component.css']
})
export class GitKrakenGettingStartedComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Getting Started");
  }

  ngOnInit() {
  }

}
