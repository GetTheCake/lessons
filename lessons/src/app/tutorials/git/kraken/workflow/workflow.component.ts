import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.css']
})
export class GitKrakenWorkflowComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Workflow");
  }

  ngOnInit() {
  }

}
