import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-what-are-branches',
  templateUrl: './what-are-branches.component.html',
  styleUrls: ['./what-are-branches.component.css']
})
export class WhatAreBranchesComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("What Are Branches?");
  }

  ngOnInit() {
  }

}
