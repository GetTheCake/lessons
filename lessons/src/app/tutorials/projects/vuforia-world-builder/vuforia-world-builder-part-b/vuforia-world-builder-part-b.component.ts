import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
    selector: 'app-vuforia-world-builder-part-b',
    templateUrl: './vuforia-world-builder-part-b.component.html',
    styleUrls: ['./vuforia-world-builder-part-b.component.css']
})
export class VuforiaWorldBuilderPartBComponent implements OnInit {
    constructor(private globals: GlobalsService) {
        globals.SetTitle("Vuforia World Builder - Part 2");
    }

    ngOnInit() {
    }

}
