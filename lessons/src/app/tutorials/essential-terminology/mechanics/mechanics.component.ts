import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-mechanics',
  templateUrl: './mechanics.component.html',
  styleUrls: ['./mechanics.component.css']
})
export class MechanicsComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Mechanics");
  }

  ngOnInit() {
  }
}
