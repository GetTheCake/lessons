import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-engine',
  templateUrl: './engine.component.html',
  styleUrls: ['./engine.component.css']
})
export class EngineComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Engine");
  }

  ngOnInit() {
  }

}
