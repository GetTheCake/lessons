import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: ['./ui.component.css']
})
export class UiComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("UI");
  }

  ngOnInit() {
  }
}
