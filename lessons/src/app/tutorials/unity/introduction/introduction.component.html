<p>
  Use this guide as a way to introduce yourself to Unity. To be honest, I just as well recommend learning this stuff from
  <a href="https://unity3d.com/learn/tutorials/topics/interface-essentials">Unity</a> themselves. I have a certain order in which I think is good easy to way to go from having zero experience in
  2D or 3D art or even code to jump in to Unity and start learning how to make interactive experiences.
</p>

<h2>
  Project Set-Up
</h2>

<p>
  The first thing you need when you try to open Unity, is a project to work in. When you don't have one, you need to make a
  new one from scratch. Here's how:
</p>

<ol>
  <li>
    <p>
      Open Unity, a project window will open, you won't have any projects to select from at the moment, so select 'New' at the
      top-right.
    </p>
    <p>
      <img src="assets/images/unity/introduction/project-nav-bar.jpg">
    </p>
  </li>
  <li>
    <p>
      At this stage, there is a small checklist to complete:
    </p>
    <ul>
      <li>Give your project a new name.</li>
      <li>For now, check that your project is 3D instead of 2D.</li>
      <li>Disable the Unity Analytics, this won't concern you just yet.</li>
      <li>
        <p>
          Make sure to set the location of your project. I suggest creating a folder on your Desktop named 'dev' and select that as
          your path. Try to place all of your Unity projects in the 'dev' folder. We can revisit this topic when we discuss
          working in Unity with git.
        </p>
      </li>
      <p>
        <img src="assets/images/unity/introduction/new-project-fields.jpg">
      </p>
      <li>Proceed to hit the 'Create Project' button at the bottom-right.</li>
    </ul>
  </li>
</ol>

<h2>
  Unity Interface
</h2>

<p>
  Unity will open with your new project ready to get started. See this link for an explanation on the Unity interface.
</p>

<ol>
  <li>
    <p>
      I will typically adjust the UI to the '2 By 3' layout.
    </p>
    <p>
      <img src="assets/images/unity/introduction/layout.jpg">
    </p>
  </li>
  <li>
    <p>
      I then set the Project pane to appear as a single column. You'll see why before long.
    </p>
    <p>
      <img src="assets/images/unity/introduction/one-column-layout.jpg">
    </p>
  </li>
</ol>

<h2>
  GameObject creation and manipulation
</h2>

<ol>
  <li>
    <p>
      At the top of your Hierarchy pane, you'll notice a 'Create' button. Click it and you should be able to find some familiar
      objects you may remember from Maya such as cameras and lights. For now, we'll make a cube and a plane. You'll find
      them in the 3D Object section.
    </p>
    <p>
      <img src="assets/images/unity/introduction/create.jpg">
    </p>
  </li>
  <li>
    <p>
      With a GameObject selected, use the W, E and R keys to alternate which gizmo you'll be using to manipulate your object. You'll
      notice they are precisely the same as Maya's, representing Translation, Rotation and Scale.
    </p>
    <p>
      <img src="assets/images/unity/introduction/gizmo.jpg">
    </p>
  </li>
  <li>
    <p>
      Keep an eye on your GameObjects in the Hierarchy pane. Note that parenting works almost exactly the same way as it does in
      Maya. Similar to how you use Maya's outliner, you can drag GameObjects into other GameObjects to parent them, and drag
      objects into an empty section of the Hierarchy pane to de-parent them.
    </p>
    <p>
      <img src="assets/images/unity/introduction/parenting.jpg">
    </p>
  </li>
</ol>

<h2>
  Scene navigation
</h2>

<p>
  Navigating around your scene, again, works much the same way as it does in Maya. Hold your Alt-Key and use the mouse buttons
  to pan, orbit and zoom.
</p>

<ul>
  <li>
    Alt + LMB: Orbit your camera around the focused region.
  </li>
  <li>
    Alt + RMB: Zoom your camera in and out.
  </li>
  <li>
    Alt + MMB: Pan your camera vertically and horizontally around your scene.
  </li>
</ul>

<p>
  When orbiting, note that you can re-focus the center around which you orbit by selecting any object and hitting the F key...
  'F' for 'focus'! This again is behaviour you'll recognize from Maya.
</p>

<p>
  There are a couple ways to navigate that are different from Maya, firstly, you can rotate your camera on the spot by simply
  holding your RMB and moving your mouse.
</p>

<p>
  While holding RMB, you can use the your W, A, S, D keys to move forward, back, left and right. Similar to how you move in
  FPS games like Doom or Overwatch. Holding Shift at the same time will allow you to move your camera faster.
</p>

<h2>
  Components
</h2>

<p>
  Now that you know how to make cubes, among other GameObjects, you might like to know that GameObjects are made out of components.
  The reason a cube appears as a cube (if you check your Inspector pane while a cube is selected) is because the GameObject
  has a MeshFilter and a MeshRenderer. MeshFilter decides which 3D mesh should appear, while the MeshRenderer decides if
  shadows are cast, and which material to use.
</p>

<p>
  <img src="assets/images/unity/introduction/components.jpg">
</p>

<p>
  If you disable or remove either one of these components, it would fail to appear. Every GameObject is really just a stack
  of components.
</p>

<ol>
  <li>
    <p>
      Try adding a RigidBody component to one of your cubes, and moving it high above your plane. Maybe rotate it a little as well...
    </p>
    <p>
      <img src="assets/images/unity/introduction/add-component.jpg">
    </p>
  </li>
  <li>
    <p>
      Run your game via the play button.
    </p>
    <p>
      <img src="assets/images/unity/introduction/play.jpg">
    </p>
  </li>
</ol>

<p>
  You'll notice that any lights you add to your scene will simply be a GameObject with a Light component added to it. Similarly,
  a Camera is just a GameObject with a Camera component added. There are tonnes of components for you to learn and master
  but knowing that they exist and that they're the real things that define a GameObject is a good start.
</p>

<h2>
  Importing Assets
</h2>

<p>
  See this link for a decent explanation of the import process. What's most important for you to know is that importing assets
  to the engine is as easy as dragging a file from Windows, any folder, and dropping them in the 'Project' pane. See your
  interface notes to find your project pane. You can import the following types of files, among many others:
</p>

<ul>
  <li>
    3D files
  </li>
  <li>
    2D Images
  </li>
  <li>
    Audio files
  </li>
  <li>
    Video files
  </li>
  <li>
    Fonts etc.
  </li>
</ul>