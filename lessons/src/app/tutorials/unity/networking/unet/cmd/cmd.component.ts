import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../../globals.service';

@Component({
  selector: 'app-cmd',
  templateUrl: './cmd.component.html',
  styleUrls: ['./cmd.component.css']
})
export class CmdComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Cmd");
    }

  ngOnInit() {
  }

}
