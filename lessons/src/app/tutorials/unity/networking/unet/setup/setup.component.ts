import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../../globals.service';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.css']
})
export class SetupComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Setup");
    }

  ngOnInit() {
  }

}
