<h2>The idea</h2>

<p>
    Now comes the time where you really have to <i>respect your flowcharts</i>. As if coding wasn't already hard enough, now you have to pay attention to where you want your code to run. On the server/host? On the client? Both?
</p>

<p>First, it's going to pay to have a small break-down of the situation.</p>

<p><img src="/assets/images/unity/networking/unet/setup/NetworkHost.png"></p>

<p>
    The image above shows our game running on 3 devices. Technically, network-dependant features don't run on clients, clients just REQUEST that they run. The server receives these requests, processes them, and returns the result back to the clients. Already, you can imagine that this 2 step process for a client to update the game state isnt instant. There's always a few millisecond delay at the least. We call this lag, or <mark>latency</mark>.
</p>

<p>
    The above diagram also illustrates the new way in which you need to treat your code. You'll be responsible for which code runs locally without any affect on anyone else, and code that needs to request action from the server. Sometimes, you may even need to run code on both as a means to pretend there's no lag.
</p>

<p>
    Now you have to get used to the idea that code you write won't run on the server without some effort on your part. Your player wants to shoot someone? Their game will have to send a request to the server for their character to fire a shot. You don't want a millisecond delay with this, so you simulate it locally as well. Now you're doubling up on code... look forward to this.
</p>

<h2>Let's get started</h2>

<p>
    Like all code that's worth knowing, this is going to be a journey, and everyone's unique path will be different. We can't master networking in this lesson, but we can begin the journey. This article will just focus on getting a result through the path of least resistance, and aim to partly explain what's happening on the way (Just get it working).
</p>

<p>I'll recommend trying this in a new Unity project so you can focus on learning the concepts.</p>

<h2>Standard player movement</h2>

<p>
    We'll practice networking by having two players able to move a character in a scene. Start by applying this code to a cube character. I tend to decorate my boxes so I can tell them apart from the background, and also tell which direction they're facing. Also provide a plane for our character to run around on.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">using</span> UnityEngine;

    <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">class</span> <span style="color:#4ec9b0;">PlayerController</span> : <span style="color:#4ec9b0;">MonoBehaviour</span> &#123;
        <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">float</span> speed <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">10</span>;
        <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">float</span> rotationSpeed <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">360</span>;

        <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">Update</span>() &#123;
            transform<span style="color:#b4b4b4;">.</span>Translate(<span style="color:#b5cea8;">0</span>, <span style="color:#b5cea8;">0</span>, <span style="color:#4ec9b0;">Input</span><span style="color:#b4b4b4;">.</span>GetAxis(<span style="color:#d69d85;">&quot;Vertical&quot;</span>) <span style="color:#b4b4b4;">*</span> speed <span style="color:#b4b4b4;">*</span> <span style="color:#4ec9b0;">Time</span><span style="color:#b4b4b4;">.</span>deltaTime);
            transform<span style="color:#b4b4b4;">.</span>Rotate(<span style="color:#b5cea8;">0</span>, <span style="color:#4ec9b0;">Input</span><span style="color:#b4b4b4;">.</span>GetAxis(<span style="color:#d69d85;">&quot;Horizontal&quot;</span>) <span style="color:#b4b4b4;">*</span> rotationSpeed <span style="color:#b4b4b4;">*</span> <span style="color:#4ec9b0;">Time</span><span style="color:#b4b4b4;">.</span>deltaTime, <span style="color:#b5cea8;">0</span>);
        &#125;
    &#125;

</pre>

<p>
    This code gives you <mark>WASD</mark> tank controls. Where <mark>A/D</mark> rotate your character, and <mark>W/S</mark> move them it forward and back. You'll notice we aren't using physics for this. Don't worry, we will, baby steps for now.
</p>

<p><img src="/assets/images/unity/networking/unet/setup/BasicMove.gif"></p>

<h2>Network Setup</h2>

<p>This is a broad topic, so this will just be a taste. Here is the most straightforward procedure I can think of to get 2 players running together as an example.</p>

<ol>
    <li>Confirm that your <mark>PlayerController</mark> character works and then turn it into a prefab. Delete the scene version of your character, we don't need it.</li>
    <li>Add 2 components to your new player prefab: <mark>NetworkTransform</mark> and <mark>NetworkIdentity</mark>.</li>
    <li>Create an empty <mark>GameObject</mark>, call it <mark>NetworkManager</mark>. Add 2 components to it. A <mark>NetworkManager</mark> and a <mark>NetworkManagerHUD</mark>.</li>
    <li>
        <p>Select your <mark>NetworkManager</mark> <mark>GameObject</mark> and drag your <mark>PlayerController</mark> prefab into <i>Spawn Info > Player Prefab</i>.</p>

        <p><img src="/assets/images/unity/networking/unet/setup/SpawnInfo.JPG"></p>
    </li>
</ol>

<h2>Trying it out...</h2>

<p>Congratulations! You just put together your first networked game! Go ahead, try it out...</p>

<p>This part's easy. Run one copy in Unity and one copy that you built-out as a Windows build.</p>

<p><img src="/assets/images/unity/networking/unet/setup/TestRun.JPG"></p>

<p>Now that you're running your game in 2 places:</p>

<ol>
    <li>In one instance, click <mark>LAN Host</mark>.</li>
    <li>
        In the other, click <mark>LAN Client</mark>. For those with some networking-know-how, It's a safe bet that we just used our machine's localhost address to host a server, and then a client application connected to it.
    </li>
</ol>