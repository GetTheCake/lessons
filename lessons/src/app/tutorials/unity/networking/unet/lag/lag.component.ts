import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../../globals.service';

@Component({
  selector: 'app-lag',
  templateUrl: './lag.component.html',
  styleUrls: ['./lag.component.css']
})
export class LagComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Lag");
    }

  ngOnInit() {
  }

}
