<p>
  If you haven't been through my previous 2 tutorials on animation, go back and read through them, they will help this tutorial make a lot more sense.
</p>

<p>
  When talking about characters, I feel like in their simplest form, they will have these aspects to them:
</p>

<ul>
  <li>Can move (left/right, etc).</li>
  <li>Animates using a series of images.</li>
</ul>

<p>
  For the sake of clear discussion, we differentiate between animation and motion. An animated asset does not necessarily move anywhere, and a moving asset is not necessarily animated. For example, a box can move left and right, animation has nothing to do with this. A humanoid character can run, with arms and legs moving back and forth, but unless some script is defining some input to allow it to move, it will animate on the spot, hence, animation is not motion, and motion is not animation.
</p>
  
<p>
  If you want a 2D, animated character, You will need to draw some images, or source them some other way. Our goal is something like this:
</p>

<p>
  <img src="assets/images/unity/animating-with-sprites/sprite-animation-example.gif">
</p>

<p>
  Resources such as the <a href="https://www.mightyape.co.nz/product/the-animators-survival-kit-paperback/20510927">Animator's Survival Kit</a> show us that there are clear, straightforward rules to animating a convincing walk cycle.
</p>

<p>
  <img src="assets/images/unity/animating-with-sprites/animator-survival-kit-walk-cycle.jpg">
</p>

<p>
  So once we know what to draw, for games, you need to know how your drawing needs to be formatted. One way or another, to
  animate, you need to draw quite a few images. Try to keep the number of frames down to a minimum. Even modern AAA games
  get away with 8-12 frames per second. An important choice you need to make is how you will import your drawings to Unity.
  You have 2 options:
</p>

<ul>
  <li>
    <p>
      Import a series of unique image files, equating to a walk cycle. This is typically bad practice.
    </p>
    <p>
      <img src="assets/images/unity/animating-with-sprites/sprites-single.jpg">
    </p>
  </li>
  <li>
    <p>
      Import a single file with a series of images on it, this is called a SpriteSheet. This is the preferred, optimized option.
    </p>
    <p>
      <img src="assets/images/unity/animating-with-sprites/sprites-multiple.jpg">
    </p>
  </li>
</ul>

<p>
  I'm going to encourage the use of sprite sheets, and will not be touching on animations built from separate, unique images.
</p>

<p>
  So you need to source a spritesheet, drawing one is beyond the scope of this tutorial, we will use sprite sheets that have already been prepared for us, but for now, you can understand that sprite sheets operate under certain rules. Many artists will have their own preferences, I prefer sprites to be laid out in a perfect grid whenever possible. This is often done manually in photoshop by painting images and then arranging them carefully. There are special tools to help:
</p>

<ul>
  <li>
    <a href="http://www.yoyogames.com/gamemaker">GameMaker</a>: Has a built-in sprite editor: See <a href="https://www.youtube.com/watch?v=r01lP59M3TQ">this video</a>
  </li>
  <li>
    <a href="https://brashmonkey.com/">Spriter</a>: A stand-alone package specially designed for all and any sprite related work
  </li>
  <li>
    <a href="https://www.codeandweb.com/texturepacker">TexturePacker</a>: A stand-alone suite, designed to take your images and pack them in to sprite sheets
  </li>
  <li>
    <a href="https://www.johnwordsworth.com/projects/photoshop-sprite-sheet-generator-script/">Sprite Sheet Generator Script</a>: A small plug-in for photoshop that will take your layers and arrange them in a grid-like fashion for sprite sheets
  </li>
</ul>

<p>For this lesson, we're gonna go ahead and import some default sprites within Unity.</p>

<ol>
  <li>
    <p>
      Import the 2D package to Unity.
    </p>
    <p>
      <img src="assets/images/unity/animating-with-sprites/2d-import.jpg">
    </p>
  </li>
  <li>
    <p>
      After importing, you'll find you have a few 2D sprites to use, I propose interest in 2 in particular for today's exercise, they are idle and run.
    </p>
    <p>
      <img src="assets/images/unity/animating-with-sprites/run-idle-project.jpg">
    </p>
    <p>
      Notice the arrow to the left of them. This implies that clicking the arrow will expand to a list.
    </p>
  </li>
  <li>
    <p>
      Expand the a RobotBoyIdle texture to see all of the sprites it contains. Select them all, and drag them to your scene view. You will see a dialogue, prompting you to save a new Animation. Save it as '<i>RobotBoyIdle' </i>and save it to your Animation folder, if you don't have one; make one.
    </p>
    <p>
      <img src="assets/images/unity/animating-with-sprites/create-anim-from-sprites.jpg">
    </p>
    <p>
      Repeat this process for RoboyBoyRun sprites.
    </p>
    <p>
      You should now see 2 sprites in your scene view, if you run your game, they should both animate. You only need one of these in your scene, the purpose of bringing both sets in was just to save the animation files. Delete the run version from your scene, leaving only the idle robot.
    </p>
  </li>
  <li>
    From following the previous tutorial on animators, you should understand that an animated object has an Animator component, this is true of your robot, which you can see by selecting it and noticing the Animator component in the inspector. In the inspector view, Double-click the AnimatorController to open the graph.
  </li>
  <li>
    <p>
      Select the state that represents your idle animation, and notice in the inspector that you're able to see which animationClip is applied to this state, click that animationClip to be taken to it's location in the project directory.
    </p>
    <p>
      <img src="assets/images/unity/animating-with-sprites/locate-anim-clip.png">
    </p>
    <p>
      This is a roundabout short-cut I tend to use to locate files when I can't be bothered searching for them. You should now be able to see the AnimationClip belonging to the state you selected. If your files are organized nicely, you should be able to see the RobotBoyRun animationClip just above the RobotBoyIdle. Drag it from the project pane in to the Animator Graph.
    </p>
  </li>
  <li>
    <p>
      The previous tutorial on Animator's describes how to create transitions. As a small refresher, right click on the <i>idle</i> state in the Animator graph, and select "Make Transition". Connect the transition to the <i>run</i> state. Your Animator window should look like this:
    </p>
    <p>
      <img src="assets/images/unity/animating-with-sprites/robot-animator.png">
    </p>
  </li>
  <li>
    <p>
      The idea here is that we have 2 states within this AnimatorController: idle and run. From the previous tutorial on Animators, you may remember that we use transitions to get an object to switch between animations. We don't want these animations to repeatedly go back and forth, run should only happen while our character is moving, idle should only happen while our character stands still. We need to add some conditions to these transitions to dictate when the transitions are allowed to occur. For conditions to exist, we need parameters.
    </p>
    <p>
      Add a parameter at the top-left of the Animator window, it will be of type <i>bool</i>. Name it '<i>isMoving</i>'.
    </p>
    <p>
      <img src="assets/images/unity/animating-with-sprites/robot-parameters.jpg">
    </p>
  </li>
  <li>
    <p>
      Great, we have a boolean parameter, now we simply need to set this up as a condition. Remember that the transition lines in the Animator graph are actually selectable. Select the transition from idle to run, in the inspector: disable <i>Exit Time. </i>Set <i>Transition Time </i>to 0. At the bottom of the inspector, you'll see a place to add conditions, add one, it should require <i>isMoving</i> to be <i>true </i>if this transition is from idle to true.
    </p>
    <p>
      <img src="assets/images/unity/animating-with-sprites/robot-conditions.jpg">
    </p>
  </li>
  <li>
    Repeat this process for the other transition from run to idle. This one should require <i>isMoving </i>to be false. Meaning if we aren't moving, we can transition to idle. At this stage, it works, you can run your game, and toggle the <i>isMoving </i>parameter on/off at the top-left of your Animator window. You should see your robot in the sceneView or gameView switching between run and idle when you toggle the isMoving variable.
  </li>
  <li>
    <p>
      The final step is to apply some code. This code will move our player left and right using the A/D or arrow keys. Upon doing so, it will tell our character's animator to toggle the <i>isMoving</i> parameter, allowing our animations to transition correctly.
    </p>
    <p>
      The script will take care of one more task. The SpriteRenderer component which you can find on our robot if you click on him; has a FlipX property that we will use to display whether he is facing left or right. The script will also take care of this.
    </p>

    <p>
      Apply this script to your robot in the scene:
    </p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">
  
  <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">float</span> speed <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">0.1f</span>;

  <span style="color:#569cd6;">private</span> <span style="color:#4ec9b0;">Animator</span> anim;
  <span style="color:#569cd6;">private</span> <span style="color:#4ec9b0;">SpriteRenderer</span> spr;
  
  <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">Start</span>() &#123;
    <span style="color:#57a64a;">// Give meaning to our anim and spr variables.</span>
    anim <span style="color:#b4b4b4;">=</span> GetComponent&lt;<span style="color:#4ec9b0;">Animator</span>&gt;();
    spr <span style="color:#b4b4b4;">=</span> GetComponent&lt;<span style="color:#4ec9b0;">SpriteRenderer</span>&gt;();
  &#125;
  
  <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">Update</span>() &#123;
    <span style="color:#57a64a;">// Add left/right input to our current position multiplied by speed.</span>
    transform<span style="color:#b4b4b4;">.</span>position <span style="color:#b4b4b4;">+=</span> <span style="color:#4ec9b0;">Vector3</span><span style="color:#b4b4b4;">.</span>right <span style="color:#b4b4b4;">*</span> <span style="color:#4ec9b0;">Input</span><span style="color:#b4b4b4;">.</span>GetAxis(<span style="color:#d69d85;">&quot;Horizontal&quot;</span>) <span style="color:#b4b4b4;">*</span> speed;
  
    <span style="color:#57a64a;">// If a horizontal button is pushed...</span>
    <span style="color:#569cd6;">if</span> (<span style="color:#4ec9b0;">Input</span><span style="color:#b4b4b4;">.</span>GetButton(<span style="color:#d69d85;">&quot;Horizontal&quot;</span>)) &#123;
      <span style="color:#57a64a;">// Set flipX on our spriteRenderer to true/false depending on if we are running left/right.</span>
      spr<span style="color:#b4b4b4;">.</span>flipX <span style="color:#b4b4b4;">=</span> <span style="color:#4ec9b0;">Input</span><span style="color:#b4b4b4;">.</span>GetAxis(<span style="color:#d69d85;">&quot;Horizontal&quot;</span>) <span style="color:#b4b4b4;">&lt;</span> <span style="color:#b5cea8;">0</span>;
    &#125;
  
    <span style="color:#57a64a;">// Set the isMoving boolean on the animator, allowing it&#39;s animations to transition correctly.</span>
    anim<span style="color:#b4b4b4;">.</span>SetBool(<span style="color:#d69d85;">&quot;isMoving&quot;</span>, <span style="color:#4ec9b0;">Input</span><span style="color:#b4b4b4;">.</span>GetButton(<span style="color:#d69d85;">&quot;Horizontal&quot;</span>));
  &#125;

</pre>
  </li>
</ol>

<p>
  You should now have a sprite-animated robot that changes facing direction and animates when you run left/right. If not, feel
  free to ask me why, or run through this tutorial again to make sure you did everything by the letter. This tutorial assumes
  you have been through the previous 2 tutorials on this topic.
</p>