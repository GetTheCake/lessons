import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-animating-with-sprites',
  templateUrl: './animating-with-sprites.component.html',
  styleUrls: ['./animating-with-sprites.component.css']
})
export class AnimatingWithSpritesComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Animating with Sprites");
  }

  ngOnInit() {
  }

}
