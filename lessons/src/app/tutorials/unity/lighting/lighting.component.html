<p>
  You know how to place objects in your scene, and may know how to re-colour them by assigning a material. To achieve a more purposeful aesthetic, you'll need to get a little more in-depth with lights, and how materials respond to these lights.
</p>

<p>
  You will come to understand the following concepts:
</p>

<ul>
  <li>Static lighting</li>
  <li>Materials</li>
  <li>Global illumination</li>
  <li>Ambient light</li>
  <li>Reflectivity</li>
  <li>Emission</li>
  <li>Dynamic lighting</li>
  <li>Skyboxes</li>
</ul>

<h2>Static Global Illumination</h2>

<ol>
  <li>
    <p>
      Notice the following scene only has 1 light in it. A directional light. However, somehow our shadows are also illuminated. What we're seeing here is ambient light. The ambient light is also bad and unrealistic. Occluded areas, such as beneath the balcony are receiving blue light from the atmosphere, this is incorrect.
    </p>
    <p>
      <img src="assets/images/unity/lighting/scene-start.jpg">
    </p>
    <p>
      Our first step will be to disable our ambient light for now. We just want to see what sort of result we're getting from the lights which we've intentionally placed. Open your LightingSettings Window by selecting <i>Window&gt;Lighting</i>. Match the following settings to start with:
    </p>
    
    <ul>
      <li>Use the Default-Skybox material.</li>
      <li>Your one directional light should be used as the sun source.</li>
      <li>We should not be using any ambient intensity at the moment.</li>
      <li>We should not be using any Reflection Intensity at the moment.</li>
      <li>We want Realtime Global Illumination (GI).</li>
      <li>
        <p>
          We'll avoid Mixed Lighting for now.
        </p>
        <p>
          <img src="assets/images/unity/lighting/settings-to-start-with.jpg">
        </p>
      </li>
    </ul>
  </li>
  <li>Save your scene!</li>
  <li>
    <p>
      In your <i>Lighting Settings </i>window, disable <i>Auto Generate </i>found near the bottom of the window.
    </p>
    <p>
      <img src="assets/images/unity/lighting/auto-generate.jpg">
    </p>
    <p>
      This is only used as a previewer which actually gives inaccurate or <i>bad</i> results.
    </p>
  </li>
  <li>
    <p>
      With your scene saved and <i>Auto Generate </i>disabled, hitting the <i>Generate Lighting</i> button will generate actual data in files, found in the same folder as your scene, and named respectively.
    </p>
    <p>
      <img src="assets/images/unity/lighting/lighting-data.jpg">
    </p>
    <p>
      You may also be of the opinion that your scene looks objectively worse now...
    </p>
    <p>
      <img src="assets/images/unity/lighting/ready-to-start.jpg">
    </p>
    <p>
      In actual fact, you've gotten rid of the automated lighting setup, and have taken your first step towards taking control of the lighting and visual impact of your scene. We're almost ready to begin lighting.
    </p>
  </li>
  <li>
    <p>
      The last thing we have to do is to grab any NON-MOVING objects, and set them as static. To do this, I parented all of my geometry to an empty GameObject named <i>env </i>(short for environment). Note that moving objects such as characters or Rigidbodies should not be included in this.
    </p>
    <p>
      <img src="assets/images/unity/lighting/my-hierarchy.jpg">
    </p>
    <p>
      Now when you select the parent-object, you can find the checkbox to set them to static in the top-right corner of the inspector. Enable this and it will prompt you to apply the change to all child-objects.
    </p>
    <p>
      <img src="assets/images/unity/lighting/set-to-static.jpg">
    </p>
  </li>
  <li>
    <p>
      In your <i>Lighting Settings </i>window, <i>Generate Lighting </i>again. This time, now that you have some static objects, note that it is taking a little longer to generate this time, you'll even be able to observe a progress gauge at the bottom-right of your Unity window.
    </p>
    <p>
      <img src="assets/images/unity/lighting/lighting-progress.jpg">
    </p>
    <p>
      Wait for that to finish and, et voila! Your scene is now much more beautiful, with only one directional light, some shadows, and now some nice Global Illumination, or <i>bounce light.</i>
    </p>
    <p>
      <img src="assets/images/unity/lighting/first-lit-example.jpg">
    </p>
    <p>
      There are definitely some problems with the quality here, but we can address those later. We've just taken our first big step into a better, more controlled aesthetic.
    </p>
  </li>
  <li>
    <p>
      Next, let's begin to colour our scene by creating several materials and applying them to our various objects.
    </p>
    
    <p>
      <img src="assets/images/unity/lighting/materials.jpg">
    </p>

    <p>
      <img src="assets/images/unity/lighting/first-lit-material-example.jpg">
    </p>

    <p>
      Notice that adjusting your ground's material colour/albedo at this point will have a profound effect on other objects in your scene, try making it bright red just for fun. You'll notice that it's bouncing light on to all other objects in your scene. This is what we're trying to achieve when we delve in to <i>Global Illumination</i>. Set it back to a more believable ground colour once you're done playing.
    </p>
  </li>
</ol>

<h2>Emission</h2>

<p>
  Lights are nice and all, but you can't express all lighting situations with lights alone, sometimes, in reality, we come
  across diffused and emissive surfaces that glow, like the bio-luminescent light on a deep-sea angler fish, or a neon sign
  in Las Vegas. For this, we need to apply emission to one of our materials.
</p>

<ol>
  <li>Create a new primitive object, like a cube, sphere or cylinder etc.</li>
  <li>Apply a new material to it, name your material <i>emissive</i> just for this example.</li>
  <li>
    <p>
      Find that material's emission value, enable it, set the colour to a vibrant red or something... then set the emissive value to something like 50... you can change this later.
    </p>
    <p>
      <img src="assets/images/unity/lighting/emission-setting.jpg">
    </p>
    <p>
      You will probably see a visual much like the following:
    </p>
    <p>
      <img src="assets/images/unity/lighting/emissive-first-result.jpg">
    </p>
  </li>
  <li>
    <p>
      Set that object to <i>static</i> and <i>Generate Lighting </i>again and you'll hopefully see a result more like this:
    </p>
    <p>
      <img src="assets/images/unity/lighting/emissive-intense-result.jpg">
    </p>
  </li>
  <li>
    <p>
      First problem? It's too intense, this is really easy to fix. Remember how we set our Emissive value to 50? Just reduce it or lower it as you see fit. You shouldn't need to <i>Generate Lighting</i> again. The lighting data stored in your scene folder takes care of this change. try for a more likely result from a glowing object of some kind, like perhaps a fluorescent or neon light.
    </p>
    <p>
      <img src="assets/images/unity/lighting/emissive-fixed-result.jpg">
    </p>
  </li>
</ol>

<h2>Reflectivity</h2>

<p>
  Remember earlier, we disabled reflectivity in our <i>Lighting Settings. </i>Well it's time to look at bringing it back, hold your horses though, we'll work through this step-by-step. We'll be using the static reflection probe method, and for this we need a couple things:
</p>

<ol>
  <li>A reflective material.</li>
  <li>An object we want to apply our reflective material to.</li>
  <li>A reflection probe.</li>
</ol>

<p>Let's fulfill this criteria:</p>

<ol>
  <li>Create a sphere, it will work best for demonstrating this.</li>
  <li>Apply a new material to it without making any adjustments to the material. Name that material "Reflective" for now.</li>
  <li>Set your object to static.</li>
  <li>Generate your lighting again, we won't have a reflective result yet, but at least our object is now visible among our other
    static objects.</li>
  <li>
    <p>
      Create a reflection probe.
    </p>
    <p>
      <img src="assets/images/unity/lighting/reflection-probe.jpg">
    </p>
    <p>
      What is this?
    </p>
    <p>
      <img src="assets/images/unity/lighting/unbaked-reflection-probe.jpg">
    </p>
    <p>
      Well, generate your lighting again and we'll see...
    </p>
    <p>
      <img src="assets/images/unity/lighting/reflection-probe-preview.jpg">
    </p>
    <p>
      The simplest way I can put this, is that it's sortof like a <i>Mirror Ball</i>.
    </p>
    <p>
      <img src="assets/images/unity/lighting/real-life-mirror-ball.jpg">
    </p>
    <p>
      However, it's a mirror ball that shares what it sees with reflective surfaces around it. We're going to try and get our Sphere GameObject to see what this ReflectionProbe can see.
    </p>
  </li>
  <li>
    <p>
      Parent the ReflectionProbe to your Sphere, and zero out the position so that it sits directly inside your sphere.
    </p>
    <p>
      <img src="assets/images/unity/lighting/reflective-hierarchy.jpg">
    </p>
    <p>
      <img src="assets/images/unity/lighting/reflection-probe-transform.jpg">
    </p>
  </li>
  <li>
    Generate your lighting again.
  </li>
  <li>
    <p>
      Now try adjusting the reflective properties of your material. You should see that you can tweak the visibility and blurriness of your reflection.
    </p>
    <p>
      <img src="assets/images/unity/lighting/adjust-reflectivity.jpg">
    </p>
    <p>
      <img src="assets/images/unity/lighting/reflection-example.jpg">
    </p>
  </li>
  <li>
    That's all there is to reflectivity for now. For your own information, try placing your ReflectionProbe in vastly different positions and generate your lighting again. You should quickly come to grips with the exact behaviour that your reflection probe is eliciting.
  </li>
</ol>

<h2>Lighting dynamic objects with LightProbes</h2>

<p>
  So, there is one problem with all the cool stuff we've learned so far... it only works on static objects. Go ahead, try to
  place a cube in your scene that isn't static, it won't pick up the bounce light, and it won't pick up the vibrant colour
  from your emissive object. This can be the most annoying concept to learn in lighting, as it's not a concept that has much
  in common with any real-world perception we may have.
</p>

<p>
  Static objects are objects that don't move. It's easy for our computer to process these objects and store information about
  them. Objects like characters and rigidbodies that tend to move around our scenes during the game can't have this information
  stored, they need dynamic lights, and the lighting we have here is not dynamic, it's static. Dynamic objects typically
  won't respond to stored lighting data. They want to respond to realtime lights instead.
</p>

<p>
  Now it's expensive for our CPU and GPU to to manage this so we don't simply try to add more dynamic lights to our scene,
  that'd be bad. Rather, we may have one or two dynamic lights for casting shadows, but the majority of our scene lighting
  will attempt to use the stored lighting data we already have. So how can we use the data we generated on a dynamic, moving
  object?
</p>

<p>
  We have to use something called light probes. Picture a bunch of dots around your scene, those dots sample the light received
  in their exact position, when a dynamic object comes into proximity of one of these dots, it shares that dot's lighting
  approximation. A dynamic object will blend between a few dots, this is how your realtime object can respond to stored lighting
  data.
</p>

<p>
  Before we get started, here is an example of the dots I mentioned, and what we'll be aiming for:
</p>

<p>
  <img src="assets/images/unity/lighting/light-probe-finished-scene.jpg">
</p>

<p>Let's get started:</p>

<ol>
  <li>
    <p>
      Create a LightProbeGroup in your scene and position it somewhere near the middle of your scene. You'll notice it appears as small collection of dots.
    </p>
    <p>
      <img src="assets/images/unity/lighting/new-reflection-probe.jpg">
    </p>
  </li>
  <li>
    <p>
      In the inspector, hit the <i>Edit </i>button.
    </p>
    <p>
      <img src="assets/images/unity/lighting/edit-light-probe.jpg">
    </p>
  </li>
  <li>
    <p>
      We need to distribute these dots in a very systematic way to begin with, we don't want to get lost in a sea of dots. First I will switch to a side view, select all dots on the left and pull them to the far-left boundary of my scene. I repeat this process for the right, front, top and bottom sides of the reflection probe as well.
    </p>

    <p>
      <img src="assets/images/unity/lighting/light-probe-first-distribution.jpg">
    </p>

    <p>
      So your 8 dots should encompass the inner boundaries of your scene, try NOT to place any dots INSIDE any objects.
    </p>
  </li>
  <li>
    <p>
      Select all of the dots at the top, and duplicate them, drag those duplicates down, distributing them at some regular interval from their original position. Repeat this until you have a nicely layered distribution of dots.
    </p>
    <p>
      <img src="assets/images/unity/lighting/light-probe-second-distribution.jpg">
    </p>
    <p>
        Repeat this process by selecting, duplicating and distributing your dots from the front view, and again from the side view until you have a nice looking three-dimensional grid across your scene.
    </p>
    <p>
      <img src="assets/images/unity/lighting/light-probe-third-distribution.jpg">
    </p>
  </li>
  <li>
    If any of your dots are sitting inside of objects, select them individually and drag them out of said objects, keep them close to that spot, as close as you can without intersecting.
  </li>
  <li>Generate your lighting again.</li>
  <li>
    <p>
      Now your dynamic (non-static) objects will be able to respond visually to the stored light data within your scene.
    </p>
    <p>
      <img src="assets/images/unity/lighting/dynamic-object.gif">
    </p>
  </li>
  <li>
    <p>
      As an added touch of beauty, you could re-introduce the ambient light at the top of your <i>Lighting Settings </i>window. This is more effective in outdoor environments.
    </p>
    <p>
      <img src="assets/images/unity/lighting/re-ambient-light.jpg">
    </p>
    <p>
      <img src="assets/images/unity/lighting/ambient-light.jpg">
    </p>
  </li>
</ol>