import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-object-pooling',
  templateUrl: './object-pooling.component.html',
  styleUrls: ['./object-pooling.component.css']
})
export class ObjectPoolingComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Object Pooling");
    }

    ngOnInit() {
    }
}
