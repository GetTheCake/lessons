<p>
        The focus of this article is optimization. If you'd like to see what Unity has to say on the matter, the following links are pretty enlightening:
    </p>

    <ul>
        <li>
            <a href="https://unity3d.com/learn/tutorials/topics/performance-optimization/optimizing-garbage-collection-unity-games">Optimizing garbage collection in Unity games</a>
        </li>
        <li><a href="https://unity3d.com/learn/tutorials/topics/performance-optimization/optimizing-scripts-unity-games?playlist=44069">Optimizing scripts in Unity games</a></li>
        <li><a href="https://unity3d.com/learn/tutorials/temas/performance-optimization/optimizing-graphics-rendering-unity-games?playlist=44069">Optimizing graphics rendering in Unity games</a></li>
    </ul>

    <h1>What is object pooling?</h1>

    <p>Object pooling is a pattern to optimize performance in cases where we're frequently spawning or destroying a particular object. Take bullets for example from Space Invaders. We know that our player needs to shoot a lot, so we code our bullets to spawn and travel, then when they're off-screen or hit a target, we destroy them. Think for a moment how in C# we have constructors and destructors: code that runs when an object is spawned or destroyed. Also that there is a thing called Garbage Collection which is basically to clean up unused memory, and to spawn/destroy, we're putting a lot of strain on this with our bullets.</p>

    <p>A better way would be to have a pool of objects (or bullets) that we draw from. We avoid spawning or destroying objects, and opt instead to show/hide them. When we shoot, we don't spawn a bullet, we simply turn a bullet on and take it out of our object pool. The bullet does it's job, we then turn it off and put it back in our pool to be used later. This solves the problems described in the previous paragraph. In this lesson we'll be seeing how that looks in practice.</p>

    <h1>First: an object worth pooling</h1>

    <p>We don't need anything fancy to try this out, but it'll illustrate some utility a bit better if we have something to work with. Here's some basic resizing functionality for our object to perform. </p>

    <pre style="font-family:Consolas;font-size:13px;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">private</span> <span style="color:#b8d7a3;">IEnumerator</span> Resize()  &#123;
        <span style="color:#569cd6;">float</span> t <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">0</span>;
        <span style="color:#569cd6;">float</span> duration <span style="color:#b4b4b4;">=</span> <span style="color:#4ec9b0;">Random</span><span style="color:#b4b4b4;">.</span>Range(<span style="color:#b5cea8;">1.0f</span>, <span style="color:#b5cea8;">3.0f</span>);
 
        <span style="color:#569cd6;">while</span> (t <span style="color:#b4b4b4;">&lt;</span> duration)  &#123;
            transform<span style="color:#b4b4b4;">.</span>localScale <span style="color:#b4b4b4;">=</span> <span style="color:#4ec9b0;">Vector3</span><span style="color:#b4b4b4;">.</span>Lerp(<span style="color:#4ec9b0;">Vector3</span><span style="color:#b4b4b4;">.</span>one, <span style="color:#4ec9b0;">Vector3</span><span style="color:#b4b4b4;">.</span>zero, resizePattern<span style="color:#b4b4b4;">.</span>Evaluate(t <span style="color:#b4b4b4;">/</span> duration));
 
            t <span style="color:#b4b4b4;">+=</span> <span style="color:#4ec9b0;">Time</span><span style="color:#b4b4b4;">.</span>deltaTime;
            <span style="color:#569cd6;">yield</span> <span style="color:#569cd6;">return</span> <span style="color:#569cd6;">null</span>;
         &#125;
 
        transform<span style="color:#b4b4b4;">.</span>localScale <span style="color:#b4b4b4;">=</span> <span style="color:#4ec9b0;">Vector3</span><span style="color:#b4b4b4;">.</span>zero;
        coroutine <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">null</span>;
     &#125;
    </pre>

    <p>Heads-up, I'm not going to explain more than I need to. These topics are weighty enough as it is. You're expected to know what to do with methods like the above and use your own knowledge to deduct the correct placement of certain elements of any given script. Anyway, the above code creates the following behaviour:</p>

    <p>
        <img src="assets/images/unity/code-practices/object-pooling/object-behaviour.gif">
    </p>

    <p>We're here to talk about object pooling, just keep in mind that the above code has nothing to do with pooling objects, it's just to add some flair to our experiment. We're going to spawn several objects with the above behaviour.</p>

    <h1>Second: spawn a lot of these guys</h1>

    <p>Let's now include some code that we call while dragging our mouse. The following script in plain English will:</p>
    <ul>
        <li>Instantiate a cube;</li>
        <li>Call a method that sets its position to our mouse-position and recolours it by randomly sampling a gradient;</li>
        <li>Call <mark>Resize()</mark> from before;</li>
        <li>Destroys the object after the resize as we no longer have any use for this object.</li>
    </ul>

    <pre style="font-family:Consolas;font-size:13px;color:gainsboro;background:#1e1e1e;">
    
    <span style="color:#569cd6;">private</span> <span style="color:#b8d7a3;">IEnumerator</span> SpawnObject()  &#123;
        <span style="color:#4ec9b0;">Transform</span> currentTransform <span style="color:#b4b4b4;">=</span> Instantiate(prefab, transform<span style="color:#b4b4b4;">.</span>position, <span style="color:#4ec9b0;">Quaternion</span><span style="color:#b4b4b4;">.</span>identity);
 
        InitializeObject(currentTransform);
 
        <span style="color:#569cd6;">yield</span> <span style="color:#569cd6;">return</span> StartCoroutine(Resize(currentTransform));
 
        Destroy(currentTransform<span style="color:#b4b4b4;">.</span>gameObject);
     &#125;
 
    <span style="color:#569cd6;">void</span> InitializeObject(<span style="color:#4ec9b0;">Transform</span> _transform)  &#123;
        <span style="color:#4ec9b0;">Vector3</span> pos <span style="color:#b4b4b4;">=</span> <span style="color:#4ec9b0;">Input</span><span style="color:#b4b4b4;">.</span>mousePosition;
        pos<span style="color:#b4b4b4;">.</span>z <span style="color:#b4b4b4;">=</span> transform<span style="color:#b4b4b4;">.</span>forward<span style="color:#b4b4b4;">.</span>z <span style="color:#b4b4b4;">-</span> <span style="color:#4ec9b0;">Camera</span><span style="color:#b4b4b4;">.</span>main<span style="color:#b4b4b4;">.</span>transform<span style="color:#b4b4b4;">.</span>forward<span style="color:#b4b4b4;">.</span>z <span style="color:#b4b4b4;">+</span> <span style="color:#b5cea8;">3</span>;
        _transform<span style="color:#b4b4b4;">.</span>position <span style="color:#b4b4b4;">=</span> <span style="color:#4ec9b0;">Camera</span><span style="color:#b4b4b4;">.</span>main<span style="color:#b4b4b4;">.</span>ScreenToWorldPoint(pos);
 
        _transform<span style="color:#b4b4b4;">.</span>GetComponent&lt;<span style="color:#4ec9b0;">Renderer</span>&gt;()<span style="color:#b4b4b4;">.</span>material<span style="color:#b4b4b4;">.</span>color <span style="color:#b4b4b4;">=</span> colourSpectrum<span style="color:#b4b4b4;">.</span>Evaluate(<span style="color:#4ec9b0;">Random</span><span style="color:#b4b4b4;">.</span>value);
     &#125;
    </pre>

    <p>
        <img src="assets/images/unity/code-practices/object-pooling/spawn-badly.gif" alt="">
    </p>

    <p>This has a somewhat pleasing result, but if we're concerned with performance, we think for a moment about how busy the Garbage Collector is when spawning and destroying so many objects repeatedly.</p>
    
    <h1>Third: take it easy on the Garbage Collector with object pooling</h1>

    <p>
        So, we refactor our code a little. Try and spot the difference:
    </p>

    <pre style="font-family:Consolas;font-size:13px;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">private</span> <span style="color:#b8d7a3;">IEnumerator</span> SpawnObject()  &#123;
        <span style="color:#4ec9b0;">Transform</span> currentTransform <span style="color:#b4b4b4;">=</span> GetCurrentTransform();

        InitializeObject(currentTransform);

        <span style="color:#569cd6;">yield</span> <span style="color:#569cd6;">return</span> StartCoroutine(Resize(currentTransform));

        DeInitializeObject(currentTransform);

        objects<span style="color:#b4b4b4;">.</span>Add(currentTransform);
     &#125;

    <span style="color:#569cd6;">private</span> <span style="color:#4ec9b0;">Transform</span> GetCurrentTransform()  &#123;
        <span style="color:#4ec9b0;">Transform</span> currentTransform;
        <span style="color:#569cd6;">if</span> (objects<span style="color:#b4b4b4;">.</span>Count <span style="color:#b4b4b4;">&lt;=</span> <span style="color:#b5cea8;">0</span>)  &#123;
            currentTransform <span style="color:#b4b4b4;">=</span> Instantiate(prefab, transform<span style="color:#b4b4b4;">.</span>position, <span style="color:#4ec9b0;">Quaternion</span><span style="color:#b4b4b4;">.</span>identity);
         &#125; <span style="color:#569cd6;">else</span>  &#123;
            currentTransform <span style="color:#b4b4b4;">=</span> objects[<span style="color:#b5cea8;">0</span>];
            objects<span style="color:#b4b4b4;">.</span>Remove(currentTransform);
         &#125;

        <span style="color:#569cd6;">return</span> currentTransform;
     &#125;
    </pre>

    <p>First, look at the familiar method we knew from before (<mark>SpawnObject()</mark>), compare it with the above. There are 3 differences we can see:</p>
    <ol>
        <li>
            <mark>currentTransform</mark> no longer instantiates, rather it runs a method to decide between instantiating or using a pooled object. It's only allowed to instantiate if there aren't any pooled objects left to use instead. Once it's instantiated enough times, it's unlikely that our pool will ever be empty again, so eventually the instantiations should slow and virtually stop altogether.
        </li>
        <li>
            We still call <mark>Resize()</mark> as we did before, but instead of destroying our object afterwards, we have a method (<mark>DeInitialize()</mark>) in-place whose job is to do whatever must be done to clear our object from the screen. In our case it's a simple matter of making the object invisible, but for more complex objects it could include tasks such as disabling rigidbodies or colliders etc.
        </li>
        <li>
            We finish up by returning the object in question to our pool, ready to be used again should the need arise.
        </li>
    </ol>

    <p>Just a quick note, we're turning objects on as we draw them from the pool, and off again as we put them back. It's probably obvious, but just so it's clear how simple this is, we're using the following line at the start of <mark>InitializeObject()</mark>:</p>

    <pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">
        
    _transform<span style="color:#b4b4b4;">.</span>gameObject<span style="color:#b4b4b4;">.</span>SetActive(<span style="color:#569cd6;">true</span>);
    </pre>

    <p>And the opposite for <mark>DeInitializeObject()</mark>:</p>

    <pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">
    
    _transform<span style="color:#b4b4b4;">.</span>gameObject<span style="color:#b4b4b4;">.</span>SetActive(<span style="color:#569cd6;">false</span>);
    </pre>

    <p>There are plenty of approaches to object pooling, this is just mine. You should be able to find a lot of other approaches to it on the web, for example: <a href="https://www.raywenderlich.com/847-object-pooling-in-unity">Object Pooling in Unity</a>.</p>

    <h1>Fourth: batching</h1>

    <p>We've taken care of garbage collection, now if we could just address one of the problems that showed up earlier. Look back at the colourful gif from before, there's something fairly alarming in there:</p>

    <p><img src="assets/images/unity/code-practices/object-pooling/bad-batches.PNG"></p>

    <p>223 batches, and saving 114 by... I have no clue... In the grand scheme of things, these numbers, while significant, are no biggie... but let's <i>be</i> better and <i>do</i> better:</p>

    <p>We're gonna take a look at dynamic batching which is achieved pretty easily. You can read Unity's say on it <a href="https://docs.unity3d.com/Manual/DrawCallBatching.html">here</a>, but the short of it is that if you have multiple objects using the same material and all low poly, Unity will do it's best to consider most of them as one object to request a visual of from our GPU.</p>

    <p>Have another look at our <mark>InitializeObject()</mark> found in the second part of this article. The last line of that method recolours our object by sampling randomly from a gradient. The problem with recolouriing per object like this is that each of our 200+ objects has it's own material and is rendered thusly. It's better if we already have numerous different-coloured materials in-place and simply assign materials when a colour change is needed, see what the method looks like now, particularly the first line and the last line since you last saw it:</p>

    <pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">
    
    <span style="color:#569cd6;">void</span> InitializeObject(<span style="color:#4ec9b0;">Transform</span> _transform)  &#123;
        _transform<span style="color:#b4b4b4;">.</span>gameObject<span style="color:#b4b4b4;">.</span>SetActive(<span style="color:#569cd6;">true</span>);

        <span style="color:#4ec9b0;">Vector3</span> pos <span style="color:#b4b4b4;">=</span> <span style="color:#4ec9b0;">Input</span><span style="color:#b4b4b4;">.</span>mousePosition;
        pos<span style="color:#b4b4b4;">.</span>z <span style="color:#b4b4b4;">=</span> transform<span style="color:#b4b4b4;">.</span>forward<span style="color:#b4b4b4;">.</span>z <span style="color:#b4b4b4;">-</span> <span style="color:#4ec9b0;">Camera</span><span style="color:#b4b4b4;">.</span>main<span style="color:#b4b4b4;">.</span>transform<span style="color:#b4b4b4;">.</span>forward<span style="color:#b4b4b4;">.</span>z <span style="color:#b4b4b4;">+</span> <span style="color:#b5cea8;">3</span>;
        _transform<span style="color:#b4b4b4;">.</span>position <span style="color:#b4b4b4;">=</span> <span style="color:#4ec9b0;">Camera</span><span style="color:#b4b4b4;">.</span>main<span style="color:#b4b4b4;">.</span>ScreenToWorldPoint(pos);

        _transform<span style="color:#b4b4b4;">.</span>GetComponent&lt;<span style="color:#4ec9b0;">Renderer</span>&gt;()<span style="color:#b4b4b4;">.</span>material <span style="color:#b4b4b4;">=</span> materials[<span style="color:#4ec9b0;">Random</span><span style="color:#b4b4b4;">.</span>Range(<span style="color:#b5cea8;">0</span>, materials<span style="color:#b4b4b4;">.</span>Length<span style="color:#b4b4b4;">-</span><span style="color:#b5cea8;">1</span>)];
     &#125;
    </pre>

    <p>And see the resulting batch and saved count:</p>

    <p>
        <img src="assets/images/unity/code-practices/object-pooling/good-batching.gif">
    </p>

    <p>I hope people can agree that 30(ish) is a smaller number than 250(ish). In any case, that's object pooling and batching done simply.</p>