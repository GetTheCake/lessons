<p>
  Lists are like arrays, but when introduced, they simplify some of the problems we have with arrays. Firstly, arrays are non-resizable, whereas lists don't require a size in order to work, so it's not a factor. Arrays take a bit of work to add or remove elements from, whereas lists just magically do that. It comes at a cost, but worry about that when performance is actually an issue. As a rule, use arrays when it's all you need.
</p>

<h2>Declaration</h2>

<p>The declaration is totally different, though initializing them is quite similar. First, we must have the <mark>System.Collections.Generic</mark> namespace. Arrays are denoted with <mark>[]</mark> after the type, whereas a list is declared which encapsulates the type it intends to be.</p>

<p>
  Instead of:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

  <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">int</span>[] myArray;

</pre>

<p>
  We do it like this:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

  <span style="color:#569cd6;">private</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt; myList;

</pre>

<p>
  You can always define this later, but it can be a good idea to provide an actual list to this variable, remember, at this point it's just an empty box that can eventually hold a list. We'll initialize this as an empty list.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

  <span style="color:#569cd6;">private</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt; myList <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt;();

</pre>

<p>
  Just like arrays, We can populate our list at this stage as well, unlike arrays, we're required to specify that it's a new list we're providing:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

  <span style="color:#569cd6;">private</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt; myList <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt;() &#123; <span style="color:#b5cea8;">4</span>, <span style="color:#b5cea8;">3</span>, <span style="color:#b5cea8;">2</span> &#125;;

</pre>

<p>
    Though we don't actually need the argument paranthesis, spot the difference:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">private</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt; myList <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt; &#123; <span style="color:#b5cea8;">4</span>, <span style="color:#b5cea8;">3</span>, <span style="color:#b5cea8;">2</span> &#125;;

</pre>

<h2>Assignment</h2>

<h3>Adding</h3>

<p>
  If you've initialized your list already, you can simply use a method to add items to the end of your list:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    myList<span style="color:#b4b4b4;">.</span>Add(<span style="color:#b5cea8;">90</span>);
    myList<span style="color:#b4b4b4;">.</span>Add(<span style="color:#b5cea8;">30</span>);
    myList<span style="color:#b4b4b4;">.</span>Add(<span style="color:#b5cea8;">88</span>);

</pre>

<p>
    You don't have to add only to the end of a list, you also jam things in, here we insert the number 99, midway through <mark>myList</mark> at the index of 4.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    myList<span style="color:#b4b4b4;">.</span>Insert(<span style="color:#b5cea8;">4</span>, <span style="color:#b5cea8;">99</span>);

</pre>

<p>If you haven't initialized your list already, or you want to wipe it and create an entirely new list, that's easy enough too:</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    myList <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt; &#123;
        <span style="color:#b5cea8;">345</span>,
        <span style="color:#b5cea8;">35</span>,
        <span style="color:#b5cea8;">333333335</span>,
        <span style="color:#b5cea8;">35</span>,
        <span style="color:#b5cea8;">35566</span>
    &#125;;

</pre>

<h3>Removing</h3>

<p>
  It's equally easy to remove elements, this code would find the first instance of 35 in the list and remove it, then resize the list appropriately, shifting higher indices down by 1:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    myList<span style="color:#b4b4b4;">.</span>Remove(<span style="color:#b5cea8;">35</span>);

</pre>

<p>
  This next line will also remove an element, but rather than telling it to find a specific element in the list, we specify which index we'd like to remove, in this case the second element.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    myList<span style="color:#b4b4b4;">.</span>RemoveAt(<span style="color:#b5cea8;">2</span>);

</pre>

<p>
    If you're about to rebuild your list's contents from scratch, it's a good idea to clear out the current contents first. This is easy:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    myList<span style="color:#b4b4b4;">.</span>Clear();

</pre>

<h2>Get Values</h2>

<p>
    You get the values in a list in precisely the same you would with an array.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(myList[<span style="color:#b5cea8;">2</span>]);

</pre>

<p>
    We get a list's size not by using <mark>.Length</mark>, that's for arrays, list sizes are acquired with <mark>.Count</mark>:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">for</span> (<span style="color:#569cd6;">int</span> i <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">0</span>; i <span style="color:#b4b4b4;">&lt;</span> myList<span style="color:#b4b4b4;">.</span>Count; i<span style="color:#b4b4b4;">++</span>) &#123;
        <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(i);
    &#125;

</pre>

<h2>Copying</h2>

<p>
  Copying the contents of one list to another is really easy. Lists have a copy constructor, meaning that you simply say which list's contents you want in your new list constructor:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt; targetList <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt;(myList);

</pre>

<h2>Pros and Cons</h2>

<p>
  Thye're quick to use without requiring too much forethought, just convenient in general. They do however have the cost of slightly more poorly optimized memory management behind the scenes. This is often not a problem but you'll usually only resort to a list when it's most appropriate.
</p>

<h2>Example</h2>

<p>
  Here's an example that puts all of the aforementioned to use. See if you can understand the way everything works:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">int</span>[] myArray;
    <span style="color:#569cd6;">private</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt; myList <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt; &#123; <span style="color:#b5cea8;">4</span>, <span style="color:#b5cea8;">3</span>, <span style="color:#b5cea8;">2</span> &#125;;
    
    <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">Awake</span>() &#123;
        myList <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt; &#123;
            <span style="color:#b5cea8;">345</span>,
            <span style="color:#b5cea8;">35</span>,
            <span style="color:#b5cea8;">333333335</span>,
            <span style="color:#b5cea8;">35</span>,
            <span style="color:#b5cea8;">35566</span>
        &#125;;

        myList<span style="color:#b4b4b4;">.</span>Add(<span style="color:#b5cea8;">90</span>);
        myList<span style="color:#b4b4b4;">.</span>Add(<span style="color:#b5cea8;">30</span>);
        myList<span style="color:#b4b4b4;">.</span>Add(<span style="color:#b5cea8;">88</span>);
        myList<span style="color:#b4b4b4;">.</span>Insert(<span style="color:#b5cea8;">4</span>, <span style="color:#b5cea8;">99</span>);
        myList<span style="color:#b4b4b4;">.</span>Remove(<span style="color:#b5cea8;">35</span>);
        myList<span style="color:#b4b4b4;">.</span>RemoveAt(<span style="color:#b5cea8;">2</span>);
        myList<span style="color:#b4b4b4;">.</span>Clear();

        <span style="color:#569cd6;">for</span> (<span style="color:#569cd6;">int</span> i <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">0</span>; i <span style="color:#b4b4b4;">&lt;</span> myList<span style="color:#b4b4b4;">.</span>Count; i<span style="color:#b4b4b4;">++</span>) &#123;
            <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(i);
        &#125;

        <span style="color:#57a64a;">// Prints 333333335.</span>
        <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(myList[<span style="color:#b5cea8;">2</span>]);
        <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(myList<span style="color:#b4b4b4;">.</span>Count);
        <span style="color:#57a64a;">// Prints 3.</span>

        <span style="color:#57a64a;">// Copy contents from one list to another.</span>
        <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt; targetList <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#4ec9b0;">List</span>&lt;<span style="color:#569cd6;">int</span>&gt;(myList);
    &#125;

</pre>