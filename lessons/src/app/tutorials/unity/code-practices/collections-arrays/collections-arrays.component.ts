import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-collections-arrays',
  templateUrl: './collections-arrays.component.html',
  styleUrls: ['./collections-arrays.component.css']
})
export class CollectionsArraysComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Collections: Arrays");
    }

  ngOnInit() {
  }

}
