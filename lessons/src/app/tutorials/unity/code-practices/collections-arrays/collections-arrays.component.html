<p>
    You should be familiar with arrays at this point. We'll be making a comparrison between
    <mark>arrays</mark>,
    <mark>lists</mark> and
    <mark>dictionaries</mark> in the context of Unity.
</p>

<p>
    You may already understand arrays quite well, but if we're drawing comparrisons, we'll bring into review a few simple fundamentals.
</p>

<h2>Declaration</h2>

<p>
    To start with, we denote an array by attaching
    <mark>[]</mark>
    <i>(square brackets)</i> to our variable's type.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">int</span>[] myInts;

</pre>

<p>
    Arrays can't have their sizes adjusted ever, they always keep the size they had when they were created. Note that we haven't
    actually created an array yet, just an empty box that wants to hold an array. We can initialize that variable to hold
    an array of 6 integers like so.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">int</span>[] myInts <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#569cd6;">int</span>[<span style="color:#b5cea8;">6</span>];

</pre>

<p>
    Any attempt to assign an integer to one of our array indices without first assigning an array to this variable would have
    resulted in a null reference exception. You don't have to initialize your array when you declare it, but then you would
    have to assign a new array to it later in your code when you did plan to use it.
</p>

<p>
    Initializing your array this way would mean the only way to populate your array would be at runtime. If you know beforehand
    what elements you would like to have populating your array, you can use a shorthand version of the
    <mark>implicitly typed</mark> format.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">int</span>[] myInts <span style="color:#b4b4b4;">=</span> &#123; <span style="color:#b5cea8;">12</span>, <span style="color:#b5cea8;">1</span>, <span style="color:#b5cea8;">33</span>, <span style="color:#b5cea8;">44</span> &#125;;
    </pre>

<p>
    This also works for temp variables that are declared midway through a method, just without the private/public access tag.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">int</span>[] moreInts <span style="color:#b4b4b4;">=</span> &#123;
        <span style="color:#b5cea8;">55</span>
    ,   <span style="color:#b5cea8;">34</span>
    ,   <span style="color:#b5cea8;">58</span>
    ,   <span style="color:#b5cea8;">888</span>
    ,   <span style="color:#b5cea8;">577</span>
    &#125;;

</pre>

<h2>Assignment</h2>

<p>
    One way to assign to an array that's been declared with, say, a size of 3, would be like this:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    myInts[<span style="color:#b5cea8;">0</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">55</span>;
    myInts[<span style="color:#b5cea8;">1</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">34</span>;
    myInts[<span style="color:#b5cea8;">2</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">58</span>;

</pre>

<p>
    If you hadn't initialized your array back during the declaration though, you would have to set that here as well, like so:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    myInts <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#569cd6;">int</span>[<span style="color:#b5cea8;">3</span>];
    myInts[<span style="color:#b5cea8;">0</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">55</span>;
    myInts[<span style="color:#b5cea8;">1</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">34</span>;
    myInts[<span style="color:#b5cea8;">2</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">58</span>;

</pre>

<p>
    You can't use the shorthand for an implicitly typed array except during declaration, you can use the long version of it though,
    this way you don't have to declare your array's size before you go ahead and add things:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    myInts <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#569cd6;">int</span>[] &#123;
        myInts[<span style="color:#b5cea8;">0</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">55</span>
    ,   myInts[<span style="color:#b5cea8;">1</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">34</span>
    ,   myInts[<span style="color:#b5cea8;">2</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">58</span>
    &#125;;

</pre>

<h2>Get Values</h2>

<p>
    Getting an array's value is simple. You just say which member of the array you want based on it's index within the array,
    starting at 0. Based on the code immediately above, the following would log
    <mark>34</mark>:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(myInts[<span style="color:#b5cea8;">1</span>]);

</pre>

<p>
    It's common to iterate through an array in a loop to access each member of the array. Notice that the size of an array can
    be accessed with the
    <mark>Length</mark> property.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">for</span> (<span style="color:#569cd6;">int</span> i <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">0</span>; i <span style="color:#b4b4b4;">&lt;</span> myInts<span style="color:#b4b4b4;">.</span>Length; i<span style="color:#b4b4b4;">++</span>) &#123;
        <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(i);
    &#125;

</pre>

<h2>Copying</h2>

<p>
    There are 2 ways that spring to mind first to copy the contents from one array to another. Either way, you need to ensure
    the size of your destination array first, to match the size of your source array.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    myInts <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#569cd6;">int</span>[moreInts<span style="color:#b4b4b4;">.</span>Length];

</pre>

<p>
    And then it's okay to copy your array using one of the two following methods, this first one requires you to specify the
    length as well:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    System<span style="color:#b4b4b4;">.</span><span style="color:#4ec9b0;">Array</span><span style="color:#b4b4b4;">.</span>Copy(moreInts, myInts, moreInts<span style="color:#b4b4b4;">.</span>Length);

</pre>

<p>
    This second method asks you where you would like to begin copying to:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    moreInts<span style="color:#b4b4b4;">.</span>CopyTo(myInts, <span style="color:#b5cea8;">0</span>);

</pre>

<h2>Pros and Cons</h2>

<p>
    Arrays are typically fairly performant, they don't require any other namespaces to use and are very quick to have up and
    running. They aren't resizable, but that means that they're well geared towards situations when you need collections
    or sets of data which you can predict the sizes of beforehand. They're a pain to use if you intend to add or remove elements
    on the fly. You typically would have to temporarily copy your array to a temporary array variable, define your initial
    array as a new array with a new set size, and copy your previous array elements from the temp variable to your initial
    array, then fill the unoccupied slots with your new elements. Removing elements from an array nicely is equally as much
    a bother. Lists solve this problem, see the next article for those.
</p>

<h2>Example</h2>

<p>
    Here's an example that puts all of the aforementioned to use. See if you can understand the way everything works:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">int</span>[] myInts <span style="color:#b4b4b4;">=</span> &#123; <span style="color:#b5cea8;">12</span>, <span style="color:#b5cea8;">1</span>, <span style="color:#b5cea8;">33</span>, <span style="color:#b5cea8;">44</span> &#125;;

    <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">Awake</span>() &#123;
        ArrayExample();
    &#125;

    <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> ArrayExample() &#123;
        myInts <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#569cd6;">int</span>[<span style="color:#b5cea8;">3</span>];
        myInts[<span style="color:#b5cea8;">0</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">55</span>;
        myInts[<span style="color:#b5cea8;">1</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">34</span>;
        myInts[<span style="color:#b5cea8;">2</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">58</span>;

        <span style="color:#569cd6;">foreach</span> (<span style="color:#569cd6;">int</span> i <span style="color:#569cd6;">in</span> myInts) &#123;
            <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(i);
        &#125;

        <span style="color:#569cd6;">for</span> (<span style="color:#569cd6;">int</span> i <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">0</span>; i <span style="color:#b4b4b4;">&lt;</span> myInts<span style="color:#b4b4b4;">.</span>Length; i<span style="color:#b4b4b4;">++</span>) &#123;
            <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(i);
        &#125;

        &#123;
            <span style="color:#569cd6;">int</span> i <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">0</span>;
            <span style="color:#569cd6;">while</span> (i <span style="color:#b4b4b4;">&lt;</span> myInts<span style="color:#b4b4b4;">.</span>Length) &#123;
                <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(i<span style="color:#b4b4b4;">++</span>);
            &#125;
        &#125;

        <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(myInts[<span style="color:#b5cea8;">2</span>]);
        <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(myInts<span style="color:#b4b4b4;">.</span>Length);

        myInts <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#569cd6;">int</span>[] &#123;
            myInts[<span style="color:#b5cea8;">0</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">55</span>
        ,   myInts[<span style="color:#b5cea8;">1</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">34</span>
        ,   myInts[<span style="color:#b5cea8;">2</span>] <span style="color:#b4b4b4;">=</span> <span style="color:#b5cea8;">58</span>
        &#125;;

        <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(myInts[<span style="color:#b5cea8;">1</span>]);

        <span style="color:#569cd6;">int</span>[] moreInts <span style="color:#b4b4b4;">=</span> &#123;
            <span style="color:#b5cea8;">55</span>
        ,   <span style="color:#b5cea8;">34</span>
        ,   <span style="color:#b5cea8;">58</span>
        ,   <span style="color:#b5cea8;">888</span>
        ,   <span style="color:#b5cea8;">577</span>
        &#125;;

        <span style="color:#57a64a;">// Copy contents from one array to another. You must ensure the size of your destination array is correct to receive the data from the source array.</span>
        myInts <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">new</span> <span style="color:#569cd6;">int</span>[moreInts<span style="color:#b4b4b4;">.</span>Length];
        <span style="color:#57a64a;">//System.Array.Copy(moreInts, myInts, moreInts.Length);</span>
        moreInts<span style="color:#b4b4b4;">.</span>CopyTo(myInts, <span style="color:#b5cea8;">0</span>);
        <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(moreInts[<span style="color:#b5cea8;">3</span>]);
    &#125;

</pre>