<p>
    Singletons are patterns, things we do in code to achieve a task that follow the same step-by-step procedure every time. In the case of a singleton, that procedure can be as simple as:
</p>

<ol>
    <li>Declaring a static class instance.</li>
    <li>Defining that instance when the game starts.</li>
    <li>Access that instance's methods and properties from anywhere in your code, even from other unrelated classes/scripts.</li>
</ol>

<h2>So, what's a singleton, and why do we want one?</h2>

<p>
    Well you will be used to doing a few things by now in Unity hopefully. You're writing a controller script that needs to access methods and variables on your <mark>Player</mark> class. First problem? You need your script to have a reference to your Player. There are a few options available to do get this reference. You could:
</p>

<ul>
    <li>
        <p>
            Give this script a field that you can fill out in the inspector:
        </p>
        <p>
            <code>public Player player;</code>
        </p>
    </li>
    <li>
        Simply use <code>GetComponent&lt;Player&gt;()</code> on it's own, this would require your script to be placed on the same object as the <mark>Player</mark>, which isn't always convenient.</li>
    <li>
        <p>
            From a different object, use something like:
        </p>
        <p>
            <code>GameObject.Find("player").GetComponent&lt;Player&gt;()</code>
        </p>
        <p>
            It just seems like too much prep work, especially as you'd have to repeat this process for any other script that also needs to access your player's variables or methods.
        </p>
    </li>
</ul>

<p>
    All of these ways are valid, but just a little convoluted or at the very least, not streamlined.
</p>

<p>
    Seeing as we only have one player character, wouldn't it make sense to just have one reference to our character that every script can access? This is what a singleton does for us.
</p>

<p>
    First you need to know about static classes and variables. If you don't, I'd suggest doing a quick read-up on them somewhere but the way I'd summarize them is <mark>shared</mark> or <mark>global</mark>. Something that's immediately accessible from anywhere in your codebase without having to get a reference to it first.
</p>

<p>
    Let's look at the following:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">using</span> UnityEngine;

    <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">class</span> <span style="color:#4ec9b0;">Player</span> : <span style="color:#4ec9b0;">MonoBehaviour</span> &#123;
        <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">static</span> <span style="color:#569cd6;">int</span> health;
        <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">static</span> <span style="color:#569cd6;">int</span> strength;
        <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">static</span> <span style="color:#569cd6;">int</span> defense;

        <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">static</span> <span style="color:#569cd6;">void</span> TakeDamage(<span style="color:#569cd6;">int</span> _damage) &#123;
            health <span style="color:#b4b4b4;">-=</span> _damage;
        &#125;

        <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">void</span> Move() &#123;
            <span style="color:#57a64a;">// Code that makes the character move... let&#39;s just pretend.</span>
        &#125;
    &#125;

</pre>

<p>
    This is not a singleton, just an example of what static variables do. Basically what you're seeing here is a <mark>Player</mark> class that has globally available properties I can access. Where you'd normally have to say:
</p>

<p>
    <code>GameObject.Find("player").GetComponent&lt;Player&gt;().TakeDamage(5);</code>
</p>

<p>
    You can now simply say:
</p>

<p>
    <code>Player.TakeDamage(5);</code>
</p>

<p>
    You could NOT however say:
</p>

<p>
    <code>Player.Move();</code>
</p>

<p>
    The reason this won't work is because <code>Move()</code> is not a static method, so you'd have to do this the long way from one of the above 3 options.
</p>

<p>Having many static or global variables is not considered good practice. So let's remove their static tags.</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">int</span> health;
    <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">int</span> strength;
    <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">int</span> defense;

    <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">void</span> TakeDamage(<span style="color:#569cd6;">int</span> _damage) &#123;
        health <span style="color:#b4b4b4;">-=</span> _damage;
    &#125;

</pre>

<p>
    They're no longer globally accessible, right? Well, we can just introduce a single variable that IS global, and assign itself to that variable.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">static</span> <span style="color:#4ec9b0;">Player</span> instance;

    <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">Awake</span>() &#123;
        instance <span style="color:#b4b4b4;">=</span> <span style="color:#569cd6;">this</span>;
    &#125;

</pre>

<p>
    Now we can access our <mark>instance</mark> field from any script, and that field will have access to all of the usual public variables and methods we might like to access. This class now behaves as a singleton, this is the singleton pattern.
</p>

<p>
    In summary, we have removed the need for references to this object. The process of removing dependencies like this is known as <mark>decoupling</mark>.
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(<span style="color:#4ec9b0;">Player</span><span style="color:#b4b4b4;">.</span>instance<span style="color:#b4b4b4;">.</span>health);
    <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(<span style="color:#4ec9b0;">Player</span><span style="color:#b4b4b4;">.</span>instance<span style="color:#b4b4b4;">.</span>strength);
    <span style="color:#4ec9b0;">Debug</span><span style="color:#b4b4b4;">.</span>Log(<span style="color:#4ec9b0;">Player</span><span style="color:#b4b4b4;">.</span>instance<span style="color:#b4b4b4;">.</span>defense);

    <span style="color:#4ec9b0;">Player</span><span style="color:#b4b4b4;">.</span>instance<span style="color:#b4b4b4;">.</span>Move();

</pre>

<p>
    One last example. Take a glance at the following image. It's a class that has one job. It takes a series of <mark>strings</mark> and displays them in UI. Well... it's a basic dialogue system. Don't bother trying to understand it, just know that it displays dialogue, and that it's been setup using those 2 lines of code from above that make it a singleton.
</p>

<p>
    <img src="assets/images/unity/code-practices/singletons/singleton-large-class.JPG" alt="">
</p>

<p>
    As I said, don't bother understanding this code. Just know that the class itself has been setup as a singleton using that simple code from before.
</p>

<p>
    Here's an example of an external class that triggers dialogue from this large class:
</p>

<pre style="font-family:Consolas;font-size:13;color:gainsboro;background:#1e1e1e;">

    <span style="color:#569cd6;">public</span> <span style="color:#569cd6;">string</span>[] dialogues;

    <span style="color:#569cd6;">private</span> <span style="color:#569cd6;">void</span> <span style="color:#569cd6;">OnMouseDown</span>() &#123;
        <span style="color:#4ec9b0;">FPsuedoSingleton</span><span style="color:#b4b4b4;">.</span>instance<span style="color:#b4b4b4;">.</span>Run(dialogues);
    &#125;

</pre>

<p>
    Simple, right? That's a rhetorical question. It is simple, and that's the beauty of singletons when you need to access a large class's properties externally. No reference getters necessary.
</p>

<p>
    <img src="assets/images/unity/code-practices/singletons/no-hands.gif" style="height: 256px">
</p>

<p>
    Singletons are named this way because they ensure that there is only one instance of some object at any one time. This is useful for things that there should only be one of, like a day/night system or main character or score system. Our singletons as described here currently... sortof.... achieve this... a complete singleton pattern would have code in-place that disallows any further instancing of our class, to delete duplicate player characters, or to prevent the second one from existing in the first place. For now, just worry about getting the pattern memorized as you see it here, you'll already get plenty of utility from it this way.
</p>
