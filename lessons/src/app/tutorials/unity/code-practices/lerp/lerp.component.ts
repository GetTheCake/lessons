import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-lerp',
  templateUrl: './lerp.component.html',
  styleUrls: ['./lerp.component.css']
})
export class LerpComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Lerp");
    }

  ngOnInit() {
  }

}
