import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-data-persistence',
  templateUrl: './data-persistence.component.html',
  styleUrls: ['./data-persistence.component.css']
})
export class DataPersistenceComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Data Persistence");
    }

  ngOnInit() {
  }

}
