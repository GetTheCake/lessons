import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-skyboxes',
  templateUrl: './skyboxes.component.html',
  styleUrls: ['./skyboxes.component.css']
})
export class SkyboxesComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Skyboxes");
  }

  ngOnInit() {
  }

}
