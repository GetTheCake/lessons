import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TutorialsRoutingModule } from './tutorials-routing.module';
import { TutorialsService } from './tutorials.service';

@NgModule({
  imports: [
    CommonModule,
    TutorialsRoutingModule
  ],
  providers: [
      TutorialsService
  ],
  declarations: []
})
export class TutorialsModule { }
