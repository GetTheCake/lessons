import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-wireframes',
  templateUrl: './wireframes.component.html',
  styleUrls: ['./wireframes.component.css']
})
export class WireframesComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Wireframes");
  }

  ngOnInit() {
  }
}
