import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-the-decision-axis',
  templateUrl: './the-decision-axis.component.html',
  styleUrls: ['./the-decision-axis.component.css']
})
export class TheDecisionAxisComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("The Decision Axis");
  }

  ngOnInit() {
  }

}
