import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-tutorializing',
  templateUrl: './tutorializing.component.html',
  styleUrls: ['./tutorializing.component.css']
})
export class TutorializingComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Tutorializing");
  }

  ngOnInit() {
  }
}
