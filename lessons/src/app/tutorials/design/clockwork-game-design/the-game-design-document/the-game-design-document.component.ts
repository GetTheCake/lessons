import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-the-game-design-document',
  templateUrl: './the-game-design-document.component.html',
  styleUrls: ['./the-game-design-document.component.css']
})
export class TheGameDesignDocumentComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("The Game Design Document");
  }

  ngOnInit() {
  }
}
