import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../globals.service';

@Component({
  selector: 'app-depth-vs-complexity',
  templateUrl: './depth-vs-complexity.component.html',
  styleUrls: ['./depth-vs-complexity.component.css']
})
export class DepthVsComplexityComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Depth Vs. Complexity");
  }

  ngOnInit() {
  }

}
