import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../../globals.service';

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.css']
})
export class GoalsComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Goals");
  }

  ngOnInit() {
  }

}
