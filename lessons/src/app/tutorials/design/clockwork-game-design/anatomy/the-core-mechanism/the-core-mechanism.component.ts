import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../../globals.service';

@Component({
  selector: 'app-the-core-mechanism',
  templateUrl: './the-core-mechanism.component.html',
  styleUrls: ['./the-core-mechanism.component.css']
})
export class TheCoreMechanismComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("The Core Mechanism");
  }

  ngOnInit() {
  }

}
