import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../../../../../globals.service';

@Component({
  selector: 'app-supporting-mechanisms',
  templateUrl: './supporting-mechanisms.component.html',
  styleUrls: ['./supporting-mechanisms.component.css']
})
export class SupportingMechanismsComponent implements OnInit {
  constructor(private globals: GlobalsService ) {
    globals.SetTitle("Supporting Mechanisms");
  }

  ngOnInit() {
  }

}
