import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
    selector: 'app-integration',
    templateUrl: './integration.component.html',
    styleUrls: ['./integration.component.css']
})
export class IntegrationComponent implements OnInit {
    constructor(private globals: GlobalsService) {
        globals.SetTitle("Firebase Database - Integration");
    }

    ngOnInit() {
    }

}
