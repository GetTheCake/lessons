import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
  selector: 'app-google',
  templateUrl: './google.component.html',
  styleUrls: ['./google.component.css']
})
export class GoogleComponent implements OnInit {
    constructor(private globals: GlobalsService ) {
        globals.SetTitle("Firebase Authentication - Google");
    }

  ngOnInit() {
  }

}
