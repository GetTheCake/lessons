import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class TutorialsService {
    constructor() { }

    hidden: boolean = true;

    Toggle() {
        this.hidden = !this.hidden;

        var hamburger = document.querySelector(".hamburger");
        hamburger.classList.toggle("is-active");
    }
}
