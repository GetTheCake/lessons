import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TutorialsComponent }                     from './tutorials.component';
import { TutorialsListComponent }                  from './tutorials-list/tutorials-list.component';
import { RecentHistoryComponent }               from './recent-history/recent-history.component';
import { WireframesComponent }                  from './design/wireframes/wireframes.component';
import { FlowchartsComponent }                  from './design/flowcharts/flowcharts.component';
import { LevelDesignComponent }                 from './design/level-design/level-design.component';
import { UiAnalysisComponent }                  from './design/ui-analysis/ui-analysis.component';
import { ClockworkGameDesignComponent }         from './design/clockwork-game-design/clockwork-game-design.component';
import { TaxonomyComponent }                    from './design/clockwork-game-design/taxonomy/taxonomy.component';
import { TheDecisionAxisComponent }             from './design/clockwork-game-design/the-decision-axis/the-decision-axis.component';
import { DepthVsComplexityComponent }           from './design/clockwork-game-design/depth-vs-complexity/depth-vs-complexity.component';
import { TheStrategyGameLearningLoopComponent } from './design/clockwork-game-design/the-strategy-game-learning-loop/the-strategy-game-learning-loop.component';
import { TheGameDesignDocumentComponent }       from './design/clockwork-game-design/the-game-design-document/the-game-design-document.component';
import { ThemesComponent }                      from './design/clockwork-game-design/themes/themes.component';
import { AnalysisComponent }                    from './design/clockwork-game-design/analysis/analysis.component';
import { PlaytestingComponent }                 from './design/clockwork-game-design/playtesting/playtesting.component';
import { TutorializingComponent }               from './design/clockwork-game-design/tutorializing/tutorializing.component';
import { PitfallsComponent }                    from './design/clockwork-game-design/pitfalls/pitfalls.component';
import { AnatomyComponent }                     from './design/clockwork-game-design/anatomy/anatomy/anatomy.component';
import { GoalsComponent }                       from './design/clockwork-game-design/anatomy/goals/goals.component';
import { SupportingMechanismsComponent }        from './design/clockwork-game-design/anatomy/supporting-mechanisms/supporting-mechanisms.component';
import { TheCoreMechanismComponent }            from './design/clockwork-game-design/anatomy/the-core-mechanism/the-core-mechanism.component';
import { TheFullCoreComponent }                 from './design/clockwork-game-design/anatomy/the-full-core/the-full-core.component';
import { UiComponent }                                  from './essential-terminology/ui/ui.component';
import { GraphicsVsAestheticsComponent }                from './essential-terminology/graphics-vs-aesthetics/graphics-vs-aesthetics.component';
import { ComplexityNotEqualToDepthOrRealismComponent }  from './essential-terminology/complexity-not-equal-to-depth-or-realism/complexity-not-equal-to-depth-or-realism.component';
import { MechanicsComponent }                           from './essential-terminology/mechanics/mechanics.component';
import { KinaestheticsComponent }                       from './essential-terminology/kinaesthetics/kinaesthetics.component';
import { EngineComponent }                              from './essential-terminology/engine/engine.component';
import { GitIntroductionComponent }                     from './git/git-introduction/git-introduction.component';
import { WhatIsGitComponent }                   from './git/what-is-git/what-is-git.component';
import { WhatAreBranchesComponent }             from './git/what-are-branches/what-are-branches.component';
import { GitGettingStartedComponent }           from './git/getting-started/getting-started.component';
import { FaqsComponent }                        from './git/faqs/faqs.component';
import { GitCLIGettingStartedComponent }        from './git/cli/getting-started/getting-started.component';
import { GitTasksSimplifiedComponent }          from './git/cli/git-tasks-simplified/git-tasks-simplified.component';
import { CommonGitCommandsComponent }           from './git/cli/common-git-commands/common-git-commands.component';
import { GitCliSshComponent }                   from './git/cli/ssh/ssh.component';
import { GitKrakenGettingStartedComponent }     from './git/kraken/getting-started/getting-started.component';
import { GitKrakenWorkflowComponent }           from './git/kraken/workflow/workflow.component';
import { GitKrakenBranchingComponent }          from './git/kraken/branching/branching.component';
import { GitKrakenSshComponent }                from './git/kraken/ssh/ssh.component';
import { GitTortoiseGettingStartedComponent }   from './git/tortoise/getting-started/getting-started.component';
import { GitTortoiseWorkflowComponent }         from './git/tortoise/workflow/workflow.component';
import { GitTortoiseBranchingComponent }        from './git/tortoise/branching/branching.component';
import { GitTortoiseSshComponent }              from './git/tortoise/ssh/ssh.component';
import { CorporatePerspectiveComponent } from './understanding-our-users/corporate-perspective/corporate-perspective.component';
import { DesigningForPlayersComponent }  from './understanding-our-users/designing-for-players/designing-for-players.component';
import { IntroductionComponent }            from './unity/introduction/introduction.component';
import { TerrainComponent }                 from './unity/terrain/terrain.component';
import { PhysicsComponent }                 from './unity/physics/physics.component';
import { AnimationComponent }               from './unity/animation/animation.component';
import { AnimatorComponent }                from './unity/animator/animator.component';
import { AnimatingWithSpritesComponent }    from './unity/animating-with-sprites/animating-with-sprites.component';
import { LightingComponent }                from './unity/lighting/lighting.component';
import { SkyboxesComponent }                from './unity/skyboxes/skyboxes.component';
import { ParticleSystemsComponent }         from './unity/particle-systems/particle-systems.component';
import { AnimationCurvesComponent }         from './unity/code-practices/animation-curves/animation-curves.component';
import { CollectionsArraysComponent }       from './unity/code-practices/collections-arrays/collections-arrays.component';
import { CollectionsDictionariesComponent } from './unity/code-practices/collections-dictionaries/collections-dictionaries.component';
import { CollectionsListsComponent }        from './unity/code-practices/collections-lists/collections-lists.component';
import { CoroutinesComponent }              from './unity/code-practices/coroutines/coroutines.component';
import { DataPersistenceComponent }         from './unity/code-practices/data-persistence/data-persistence.component';
import { EventsComponent }                  from './unity/code-practices/events/events.component';
import { InverseLerpComponent }             from './unity/code-practices/inverse-lerp/inverse-lerp.component';
import { LerpComponent }                    from './unity/code-practices/lerp/lerp.component';
import { SingletonsComponent }              from './unity/code-practices/singletons/singletons.component';
import { ObjectPoolingComponent }           from './unity/code-practices/object-pooling/object-pooling.component';
import { VuforiaGettingStartedComponent }   from './unity/ar/vuforia/getting-started/getting-started.component';
import { VuforiaTrackingEventsComponent }   from './unity/ar/vuforia/tracking-events/tracking-events.component';
import { SetupComponent }                   from './unity/networking/unet/setup/setup.component';
import { TroubleshootingComponent }         from './unity/networking/unet/troubleshooting/troubleshooting.component';
import { LagComponent }                     from './unity/networking/unet/lag/lag.component';
import { RigidbodiesComponent }             from './unity/networking/unet/rigidbodies/rigidbodies.component';
import { CmdComponent }                     from './unity/networking/unet/cmd/cmd.component';
import { ClientRpcComponent }               from './unity/networking/unet/client-rpc/client-rpc.component';
import { VuforiaWorldBuilderPartAComponent }    from './projects/vuforia-world-builder/vuforia-world-builder-part-a/vuforia-world-builder-part-a.component';
import { VuforiaWorldBuilderPartBComponent }    from './projects/vuforia-world-builder/vuforia-world-builder-part-b/vuforia-world-builder-part-b.component';
import { VuforiaWorldBuilderPartCComponent }    from './projects/vuforia-world-builder/vuforia-world-builder-part-c/vuforia-world-builder-part-c.component';
import { VuforiaWorldBuilderPageMenuComponent } from './projects/vuforia-world-builder/page-vuforia-world-builder-menu/vuforia-world-builder-page-menu.component';
import { EmailComponent }                      from './android-studio/firebase/authentication/email/email.component';
import { FirebaseAuthGettingStartedComponent } from './android-studio/firebase/authentication/firebase-auth-getting-started/firebase-auth-getting-started.component';
import { GoogleComponent } from './android-studio/firebase/authentication/google/google.component';
import { FacebookComponent } from './android-studio/firebase/authentication/facebook/facebook.component';
import { BasicsComponent } from './android-studio/firebase/database/basics/basics.component';
import { IntegrationComponent } from './android-studio/firebase/database/integration/integration.component';

const routes: Routes = [
  { path: 'tutorials', redirectTo: "/tutorials/(tutorials-content:recent-history)", pathMatch: 'full' },
  { path: 'tutorials', component: TutorialsComponent, children: [
    { path: 'recent-history', component: RecentHistoryComponent, outlet: 'tutorials-content' },
    { path: 'design>wireframes', component: WireframesComponent, outlet: 'tutorials-content' },
    { path: 'design>flowcharts', component: FlowchartsComponent, outlet: 'tutorials-content' },
    { path: 'design>level-design', component: LevelDesignComponent, outlet: 'tutorials-content' },
    { path: 'design>ui-analysis', component: UiAnalysisComponent, outlet: 'tutorials-content' },
        { path: 'design>clockwork-game-design', component: ClockworkGameDesignComponent, outlet: 'tutorials-content' },
            { path: 'design>clockwork-game-design>taxonomy', component: TaxonomyComponent, outlet: 'tutorials-content' },
            { path: 'design>clockwork-game-design>the-decision-axis', component: TheDecisionAxisComponent, outlet: 'tutorials-content' },
            { path: 'design>clockwork-game-design>depth-vs-complexity', component: DepthVsComplexityComponent, outlet: 'tutorials-content' },
            { path: 'design>clockwork-game-design>the-strategy-game-learning-loop', component: TheStrategyGameLearningLoopComponent, outlet: 'tutorials-content' },
            { path: 'design>clockwork-game-design>the-game-design-document', component: TheGameDesignDocumentComponent, outlet: 'tutorials-content' },
            { path: 'design>clockwork-game-design>themes', component: ThemesComponent, outlet: 'tutorials-content' },
            { path: 'design>clockwork-game-design>analysis', component: AnalysisComponent, outlet: 'tutorials-content' },
            { path: 'design>clockwork-game-design>playtesting', component: PlaytestingComponent, outlet: 'tutorials-content' },
            { path: 'design>clockwork-game-design>tutorializing', component: TutorializingComponent, outlet: 'tutorials-content' },
            { path: 'design>clockwork-game-design>pitfalls', component: PitfallsComponent, outlet: 'tutorials-content' },
            { path: 'design>clockwork-game-design>anatomy', component: AnatomyComponent, outlet: 'tutorials-content' },
                { path: 'design>clockwork-game-design>anatomy>goals', component: GoalsComponent, outlet: 'tutorials-content' },
                { path: 'design>clockwork-game-design>anatomy>supporting-mechanisms', component: SupportingMechanismsComponent, outlet: 'tutorials-content' },
                { path: 'design>clockwork-game-design>anatomy>the-core-mechanism', component: TheCoreMechanismComponent, outlet: 'tutorials-content' },
                { path: 'design>clockwork-game-design>anatomy>the-full-core', component: TheFullCoreComponent, outlet: 'tutorials-content' },
    { path: 'essential-terminology>ui', component: UiComponent, outlet: 'tutorials-content' },
    { path: 'essential-terminology>graphics-vs-aesthetics', component: GraphicsVsAestheticsComponent, outlet: 'tutorials-content' },
    { path: 'essential-terminology>complexity-not-equal-to-depth-or-realism', component: ComplexityNotEqualToDepthOrRealismComponent, outlet: 'tutorials-content' },
    { path: 'essential-terminology>mechanics', component: MechanicsComponent, outlet: 'tutorials-content' },
    { path: 'essential-terminology>kinaesthetics', component: KinaestheticsComponent, outlet: 'tutorials-content' },
    { path: 'essential-terminology>engine', component: EngineComponent, outlet: 'tutorials-content' },
    { path: 'git>introduction', component: GitIntroductionComponent, outlet: 'tutorials-content' },
    { path: 'git>what-is-git', component: WhatIsGitComponent, outlet: 'tutorials-content' },
    { path: 'git>what-are-branches', component: WhatAreBranchesComponent, outlet: 'tutorials-content' },
    { path: 'git>getting-started', component: GitGettingStartedComponent, outlet: 'tutorials-content' },
    { path: 'git>faqs', component: FaqsComponent, outlet: 'tutorials-content' },
        { path: 'git>cli>getting-started', component: GitCLIGettingStartedComponent, outlet: 'tutorials-content' },
        { path: 'git>cli>tasks-simplified', component: GitTasksSimplifiedComponent, outlet: 'tutorials-content' },
        { path: 'git>cli>common-git-commands', component: CommonGitCommandsComponent, outlet: 'tutorials-content' },
        { path: 'git>cli>ssh', component: GitCliSshComponent, outlet: 'tutorials-content' },
        { path: 'git>kraken>getting-started', component: GitKrakenGettingStartedComponent, outlet: 'tutorials-content' },
        { path: 'git>kraken>workflow', component: GitKrakenWorkflowComponent, outlet: 'tutorials-content' },
        { path: 'git>kraken>branching', component: GitKrakenBranchingComponent, outlet: 'tutorials-content' },
        { path: 'git>kraken>ssh', component: GitKrakenSshComponent, outlet: 'tutorials-content' },
        { path: 'git>tortoise>getting-started', component: GitTortoiseGettingStartedComponent, outlet: 'tutorials-content' },
        { path: 'git>tortoise>workflow', component: GitTortoiseWorkflowComponent, outlet: 'tutorials-content' },
        { path: 'git>tortoise>branching', component: GitTortoiseBranchingComponent, outlet: 'tutorials-content' },
        { path: 'git>tortoise>ssh', component: GitTortoiseSshComponent, outlet: 'tutorials-content' },
    { path: 'understanding-our-users>corporate-perspective', component: CorporatePerspectiveComponent, outlet: 'tutorials-content' },
    { path: 'understanding-our-users>designing-for-players', component: DesigningForPlayersComponent, outlet: 'tutorials-content' },
    { path: 'unity>introduction', component: IntroductionComponent, outlet: 'tutorials-content' },
    { path: 'unity>terrain', component: TerrainComponent, outlet: 'tutorials-content' },
    { path: 'unity>physics', component: PhysicsComponent, outlet: 'tutorials-content' },
    { path: 'unity>animation', component: AnimationComponent, outlet: 'tutorials-content' },
    { path: 'unity>animator', component: AnimatorComponent, outlet: 'tutorials-content' },
    { path: 'unity>animating-with-sprites', component: AnimatingWithSpritesComponent, outlet: 'tutorials-content' },
    { path: 'unity>lighting', component: LightingComponent, outlet: 'tutorials-content' },
    { path: 'unity>skyboxes', component: SkyboxesComponent, outlet: 'tutorials-content' },
    { path: 'unity>particle-systems', component: ParticleSystemsComponent, outlet: 'tutorials-content' },
        { path: "unity>code-practices>animation-curves", component: AnimationCurvesComponent, outlet: 'tutorials-content' },
        { path: "unity>code-practices>collections-arrays", component: CollectionsArraysComponent, outlet: 'tutorials-content' },
        { path: "unity>code-practices>collections-dictionaries", component: CollectionsDictionariesComponent, outlet: 'tutorials-content' },
        { path: "unity>code-practices>collections-lists", component: CollectionsListsComponent, outlet: 'tutorials-content' },
        { path: "unity>code-practices>coroutines", component: CoroutinesComponent, outlet: 'tutorials-content' },
        { path: "unity>code-practices>data-persistence", component: DataPersistenceComponent, outlet: 'tutorials-content' },
        { path: "unity>code-practices>events", component: EventsComponent, outlet: 'tutorials-content' },
        { path: "unity>code-practices>inverse-lerp", component: InverseLerpComponent, outlet: 'tutorials-content' },
        { path: "unity>code-practices>lerp", component: LerpComponent, outlet: 'tutorials-content' },
        { path: "unity>code-practices>singletons", component: SingletonsComponent, outlet: 'tutorials-content' },
        { path: "unity>code-practices>object-pooling", component: ObjectPoolingComponent, outlet: 'tutorials-content' },
                { path: "unity>ar>vuforia>getting-started", component: VuforiaGettingStartedComponent, outlet: 'tutorials-content' },
                { path: "unity>ar>vuforia>tracking-events", component: VuforiaTrackingEventsComponent, outlet: 'tutorials-content' },
    { path: "unity>networking>unet>setup", component: SetupComponent, outlet: 'tutorials-content' },
    { path: "unity>networking>unet>troubleshooting", component: TroubleshootingComponent, outlet: 'tutorials-content' },
    { path: "unity>networking>unet>lag", component: LagComponent, outlet: 'tutorials-content' },
    { path: "unity>networking>unet>rigidbodies", component: RigidbodiesComponent, outlet: 'tutorials-content' },
    { path: "unity>networking>unet>cmd", component: CmdComponent, outlet: 'tutorials-content' },
    { path: "unity>networking>unet>client-rpc", component: ClientRpcComponent, outlet: 'tutorials-content' },
    { path: 'projects>vuforia-world-builder>part-a', component: VuforiaWorldBuilderPartAComponent, outlet: 'tutorials-content' },
    { path: 'projects>vuforia-world-builder>part-b', component: VuforiaWorldBuilderPartBComponent, outlet: 'tutorials-content' },
    { path: 'projects>vuforia-world-builder>part-c', component: VuforiaWorldBuilderPartCComponent, outlet: 'tutorials-content' },
    { path: 'android-studio>firebase>authentication>email', component: EmailComponent, outlet: 'tutorials-content' },
    { path: 'android-studio>firebase>authentication>firebase-auth-getting-started', component: FirebaseAuthGettingStartedComponent, outlet: 'tutorials-content' },
    { path: 'android-studio>firebase>authentication>google', component: GoogleComponent, outlet: 'tutorials-content' },
    { path: 'android-studio>firebase>authentication>facebook', component: FacebookComponent, outlet: 'tutorials-content' },
    { path: 'android-studio>firebase>database>basics', component: BasicsComponent, outlet: 'tutorials-content' },
    { path: 'android-studio>firebase>database>integration', component: IntegrationComponent, outlet: 'tutorials-content' }
]},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  exports: [
    RouterModule
  ],
  declarations: [
    TutorialsListComponent,
    TutorialsComponent,
    RecentHistoryComponent,
    WireframesComponent,
    FlowchartsComponent,
    LevelDesignComponent,
    UiAnalysisComponent,
    ClockworkGameDesignComponent,
    TaxonomyComponent,
    TheDecisionAxisComponent,
    DepthVsComplexityComponent,
    TheStrategyGameLearningLoopComponent,
    TheGameDesignDocumentComponent,
    ThemesComponent,
    AnalysisComponent,
    PlaytestingComponent,
    TutorializingComponent,
    PitfallsComponent,
    AnatomyComponent,
    GoalsComponent,
    SupportingMechanismsComponent,
    TheCoreMechanismComponent,
    TheFullCoreComponent,
    UiComponent,
    GraphicsVsAestheticsComponent,
    ComplexityNotEqualToDepthOrRealismComponent,
    MechanicsComponent,
    KinaestheticsComponent,
    EngineComponent,
    GitIntroductionComponent,
    WhatIsGitComponent,
    WhatAreBranchesComponent,
    GitGettingStartedComponent,
    FaqsComponent,
    GitCLIGettingStartedComponent,
    GitTasksSimplifiedComponent,
    CommonGitCommandsComponent,
    GitCliSshComponent,
    GitKrakenGettingStartedComponent,
    GitKrakenWorkflowComponent,
    GitKrakenBranchingComponent,
    GitKrakenSshComponent,
    GitTortoiseGettingStartedComponent,
    GitTortoiseWorkflowComponent,
    GitTortoiseBranchingComponent,
    GitTortoiseSshComponent,
    CorporatePerspectiveComponent,
    DesigningForPlayersComponent,
    IntroductionComponent,
    TerrainComponent,
    PhysicsComponent,
    AnimationComponent,
    AnimatorComponent,
    AnimatingWithSpritesComponent,
    LightingComponent,
    SkyboxesComponent,
    ParticleSystemsComponent,
    AnimationCurvesComponent,
    CollectionsArraysComponent,
    CollectionsDictionariesComponent,
    CollectionsListsComponent,
    CoroutinesComponent,
    DataPersistenceComponent,
    EventsComponent,
    InverseLerpComponent,
    LerpComponent,
    SingletonsComponent,
    ObjectPoolingComponent,
    VuforiaGettingStartedComponent,
    VuforiaTrackingEventsComponent,
    SetupComponent,
    TroubleshootingComponent,
    LagComponent,
    RigidbodiesComponent,
    CmdComponent,
    ClientRpcComponent,
    VuforiaWorldBuilderPartAComponent,
    VuforiaWorldBuilderPartBComponent,
    VuforiaWorldBuilderPartCComponent,
    VuforiaWorldBuilderPageMenuComponent,
    FirebaseAuthGettingStartedComponent,
    EmailComponent,
    GoogleComponent,
    FacebookComponent,
    BasicsComponent,
    IntegrationComponent
  ]
})
export class TutorialsRoutingModule { }
