<p>With so many types or people to think about in the world, it'd be really nice if we had some way to generally prescribe something
  that they could be looking for in a game. Enter <a href="https://en.wikipedia.org/wiki/Bartle_taxonomy_of_player_types">Bartle's Taxonomy</a>.
</p>
<p>
  <img src="assets/images/understanding-our-users/designing-for-players/bartles-taxonomy-chart.png">
</p>
<p>
  <b>Achievers</b> play for in-game goals. Beating levels, raising stats, holding high scores or having lots of gold. They want
  to use the features we lay out for them, and they want to be the best at using them. For these guys, designers create goals
  that go beyond what most players are expected to do like 100% completion goals, new game plus etc. Trophies and achievements
  entered the scene with these kinds of players in mind.
</p>
<p>
  <b>Explorers</b> like to discover. Not just in terms of environment but in terms of fully understanding intricate systems
  and exploring all possibilities of how to use and misuse any given system. They're the ones who push our level design to
  it's limits, purposely walking to parts of the game we thought we made inaccessible. Finding out what happens when they
  endeavor to use game features in ways we never intended. The actual game-play is just a tool to enable their exploration.
  They're not here to master the game, they only need to learn it well enough to explore unimpeded. Easter eggs, hidden areas,
  layered systems or large customization systems are all factors that draw explorers to a game.
</p>
<p>
  <b>Socializers</b> like the interpersonal aspects of the game. They're often found trading, chatting in town, forming guilds
  or helping new players just for the fun of meeting new people. They tend to gravitate towards the sorts of games that mimic
  those kinds of experiences. Features that facilitate making social connections will draw them to play. At the time of writing
  his theory, Bartle didn't have things like Twitch or Youtube. Services like these have enabled the socializer significantly
  in the modern age.
</p>
<p>
  <b>Killers</b> derive joy from asserting dominance in games, often in the form of causing misery to other players. This is
  more about owning a contest rather than the virtual act of killing something. This could mean beating someone in a contest
  of skill, holding the monopoly on certain items in the auction house or being the leader of the most well-known, successful
  guild. The more a game's features allows them to assert their dominance, the more these kinds of players enjoy it.
</p>
<p>Bartles graph has two axis, one that goes from acting to interacting, and the other which goes from players to world.</p>
<p>
  <img src="assets/images/understanding-our-users/designing-for-players/bartles-taxonomy-chart.png">
</p>
<p><b>Acting</b> means to act <b>on</b>, <b>interacting</b> means to act <b>with</b>. <b>Players</b> refers to other people in the game, while <b>world</b> is the game itself.</p>
<p><b>Achievers</b> like <b>acting on</b> the <b>world</b>.</p>
<p><b>Explorers</b> like <b>interacting with</b> the <b>world</b>.</p>
<p><b>Socializers</b> like <b>interacting with players</b>.</p>
<p><b>Killers</b> like acting <b>onplayers</b>.</p>
<p>Rather than asking if X feature is suitable for Y player type, which forces you to examine each group individually, you can
  instead, assess whether a change or decision you've made will require more acting
  <b>with</b> the world, or more acting
  <b>on</b>the world, see where that charts on the graph, then consider which group that decision will appeal most to. Games
  can lose depth or 'fun' when the game caters too much for one kind.</p>
<p>
  <iframe class="youtube" src="https://www.youtube.com/embed/1drDuaQXm_U?wmode=opaque" frameborder="0" allowfullscreen=""></iframe>
</p>
<p>Let's look at 2 user analysis roles to fill quite early on in a production.</p>
<ul>
  <li>You have an idea.</li>
  <li>You want an idea.</li>
</ul>
<p>Here are some questions you can ask when you already have an idea:</p>
<ul>
  <li>Will I have access to good sources of reference to support this idea?</li>
  <li>Does my idea confine me to any sort of genre? Yes? No? Which one?</li>
  <li>What personal life experiences can I relate to this idea? Is there a high probability that others will relate?</li>
  <li>Would it be enlightening? Or simply obscure?</li>
  <li>Who's playing similar games? What are their positive and negative responses?</li>
  <li>Is it bringing something new, or offering more of the same to people who want more of the same?</li>
  <li>Is it something that aligns with my personal experience and passion, enough to make me excited to see it through?</li>
</ul>
<p>And some questions when you don't have an idea but need one:</p>
  <ul>
    <li>What's a dominant genre on the market right now?</li>
    <li>Is that due to a small selection of games that are doing particularly well, or are a high volume of games under that
      genre doing well?</li>
    <li>Are there some types of games that haven't been explored for quite some time?</li>
    <li>Have companies let their users down in any way over the past few years (Path of Exile Vs Diablo 3).</li>
    <li>Can you tag along for the gold-mine on a suddenly popular genre of game?</li>
    <li>Could you salvage and save a type of game that hasn't been doing too well lately?</li>
    <li>What payment methods are proving effective, and in what context?</li>
  </ul>
<p>I could have thrown up pages and pages of questions, but the point being is that it's fine to start a project with an idea
  or without an idea. You just have to ask different questions.</p>
<p>The methods available to find the answers to these questions are actually right in front of us and you probably already do
  it. You do this online by simply perusing the forums and the countless studies that already get released on sales and user
  responses.
</p>
<p>See the following:</p>
<ul>
  <li><a href="https://www.appannie.com/">App Annie</a></li>
  <li><a href="https://play.google.com/store/apps/details?id=com.supercell.hayday&amp;hl=en">Google Play reviews</a></li>
  <li><a href="http://store.steampowered.com/app/243020/">Steam Reviews</a></li>
  <li><a href="https://www.glassdoor.com/index.htm">Glass Door</a></li>
</ul>
<p>The rest would be along the lines of what you already know and practice, like user surveys and interviews and the like.
</p>