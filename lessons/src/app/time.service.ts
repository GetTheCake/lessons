import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class TimeService {
    constructor() { }

    currentTime: number;
    previousTime: any = new Date();
    time = 0;
    deltaTime = 0;

    GetTime = () => {
        this.currentTime = Date.now();
        this.deltaTime = this.currentTime - this.previousTime;
        this.previousTime = this.currentTime;
        this.time += (this.deltaTime * 60) / 100000;
        return this.time;
    }
}
