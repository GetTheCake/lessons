import { Component, OnInit } from '@angular/core';
import { GlobalsService } from '../globals.service';
import { AppsService } from './apps.service';

@Component({
    selector: 'app-apps',
    templateUrl: './apps.component.html',
    styleUrls: ['./apps.component.css']
})
export class AppsComponent implements OnInit {
    constructor(private _globals: GlobalsService, _appsService: AppsService) {
        this.globals = _globals;
        this.appsService = _appsService;
    }
    
    globals : GlobalsService;
    appsService: AppsService;
    
    ngOnInit() {
        // this.globals.SetTitle("Apps");
        this.globals.SetPositionByNav();
        this.appsService.hidden = true;
        // this.appsService.Toggle();
    }
}
