import { Component, OnInit } from '@angular/core';
import { GlobalsService } from 'src/app/globals.service';

@Component({
    selector: 'app-sounbath-graded',
    templateUrl: './sounbath-graded.component.html',
    styleUrls: ['./sounbath-graded.component.css']
})
export class SounbathGradedComponent implements OnInit {
    constructor(private _globals: GlobalsService) {
        _globals.SetTitle("Soundbath (Graded)");
        this.isAuthenticated = false;
    }

    isAuthenticated: boolean;;

    ngOnInit() {
    }

    ngAfterViewInit() {
        (<HTMLInputElement>document.getElementById("password")).onkeypress = (e) => {
            if (e.keyCode == 13) {
                this.test();
            }
        }
    }

    public test() {
        var inputField = (<HTMLInputElement>document.getElementById("password"))
        var password = inputField.value;

        this.isAuthenticated = password == "marzena1001";
        inputField.value = '';
    }
}
