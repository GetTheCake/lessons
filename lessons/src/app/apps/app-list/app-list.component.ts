import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppsService } from '../apps.service';
import { JsonService } from '../../json.service';

@Component({
    selector: 'app-app-list',
    templateUrl: './app-list.component.html',
    styleUrls: ['./app-list.component.css']
})
export class AppListComponent implements OnInit {
    constructor(private router: Router, _appsService: AppsService, _jsonService: JsonService) {
        this.appsService = _appsService;
        
        _jsonService.readJson("assets/json/apps.json").subscribe(data => {
            this.jsonData = data;
        });
    }
    
    appsService: AppsService;
    jsonData: any;

    ngOnInit() {
    }

    showApp(id) {
        this.router.navigate(['/apps', { outlets: { 'app-content': [id] } }]);
        this.appsService.Toggle();
    }
}
