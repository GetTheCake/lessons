import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { GlobalsService } from '../globals.service';
import * as THREE from 'three';
// import { OrbitControls } from 'three-orbitcontrols-ts';
// import GLTFLoader from 'three-gltf-loader'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
    @ViewChild('threejs') input: ElementRef;
    paddingTop: any;

    constructor(private globals: GlobalsService) {
        globals.SetTitle("Home");
        this.time = globals.timeService;
    }

    ngOnInit() {
    }
    time: any;

    // Scene
    el: Element;
    scene: THREE.Scene;
    camera: THREE.PerspectiveCamera;
    renderer: THREE.WebGLRenderer;
    cube: THREE.Mesh;
    goolemMaterial: any;
    controls: OrbitControls;

    ngAfterViewInit() {
        this.input.nativeElement.focus();
        this.el = this.input.nativeElement;

        this.SetRenderer();
        this.SetCamera();

        this.SetWindow();
        window.addEventListener('resize', () => {
            this.SetWindow();
        }, false);
        
        this.SetScene();
        this.SetControls();

        // LIGHTS
        this.AddAmbientLight();
        this.AddDirectionalLight();
        this.AddPrimaryPointLight();
        this.AddSecondaryPointLight();

        // GEOMETRY
        this.cube = this.CreateCube();
        // this.CreatePlane();
        this.CreateLine();
        this.Create3dText();
        this.CreateCustomModel();

        // Render or Animate loop.
        var animate = () => {
            this.time.GetTime();

            // Spin the cube.
            this.cube.rotation.x += 0.01;
            this.cube.rotation.y += 0.01;

            // Adjust golem emissive.
            // Remap the -1:1 sine wave to 0-1 with the formula: (t - low) / (high - low)
            var t = (Math.sin(this.time.time * 4) - -1) / (1 - -1);
            // t * t is a slow lerp that spikes quickly.
            if (this.goolemMaterial) {
                this.goolemMaterial.emissiveIntensity = THREE.MathUtils.lerp(0, 1, t * t);
            }

            this.controls.update();
            requestAnimationFrame(animate);
            this.renderer.render(this.scene, this.camera);
        }

        animate();
    }

    private CreateCustomModel() {
        var loader = new GLTFLoader();
        loader.load('assets/gltf/goolem/Goolem.gltf', (gltf) => {
            gltf.scene.position.set(0, -2, 0);

            var texture = new THREE.TextureLoader().load("assets/gltf/goolem/blinn2_baseColor.jpg");
            
            // Material.
            this.goolemMaterial = new THREE.MeshStandardMaterial({color: 0x555555});
            this.goolemMaterial.map = texture;
            this.goolemMaterial.map.flipY = false;
            this.goolemMaterial.metalness = 0.47;
            this.goolemMaterial.roughness = 0.3;
            this.goolemMaterial.emissive = new THREE.Color(0x00ff00);
            this.goolemMaterial.emissiveIntensity = 1;
            
            // Mesh properties.
            var mesh: THREE.Mesh = <THREE.Mesh>gltf.scene.children[0];
            mesh.material = this.goolemMaterial;
            mesh.receiveShadow = true;
            mesh.castShadow = true;

            this.scene.add(gltf.scene);
        }, (xhr) => {
            console.log((xhr.loaded / xhr.total * 100) + '% loaded');
        }, (error) => {
            console.log(error);
        });
    }

    private SetScene() {
        this.scene = new THREE.Scene();
        this.scene.position.y += 0.5;
    }

    private SetCamera() {
        this.camera = new THREE.PerspectiveCamera(75, this.el.clientWidth / this.el.clientHeight, 0.1, 1000);
        this.camera.position.set(0, 0, 5);
        this.camera.lookAt(0, 0, 0);
    }

    private SetRenderer() {
        this.renderer = new THREE.WebGLRenderer({ antialias: true });
        this.el.appendChild(this.renderer.domElement);
        this.renderer.setSize(this.el.clientWidth, this.el.clientHeight);
        this.renderer.domElement.style.display = "block";

        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = THREE.BasicShadowMap;
    }

    private SetControls() {
        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        console.log(this.controls);
        this.controls.autoRotate = true;
        this.controls.enablePan = false;
        this.controls.autoRotateSpeed = 10;
        this.controls.minDistance = 5;
        this.controls.maxDistance = 10;
    }

    private SetWindow() {
        this.camera.aspect = this.el.clientWidth / this.el.clientHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(this.el.clientWidth, this.el.clientHeight);
    }

    private CreateCube = () => {
        var geometry: THREE.Geometry = new THREE.BoxGeometry(1, 1, 1);
        var material: THREE.Material = new THREE.MeshPhongMaterial({
            color: 0xff3c00
        });
        var mesh: THREE.Mesh = new THREE.Mesh(geometry, material);
        mesh.castShadow = true;
        this.scene.add(mesh);

        return mesh;
    }

    private CreatePlane() {
        var material: THREE.Material = new THREE.MeshPhongMaterial({
            color: 0x6C6C6C
        });
        var mesh = new THREE.Mesh(new THREE.PlaneGeometry(5, 5), material);
        mesh.position.y -= 3;
        mesh.rotation.x = -Math.PI / 2;
        mesh.receiveShadow = true;
        this.scene.add(mesh);
    }

    private CreateLine() {
        var material: THREE.Material = new THREE.LineBasicMaterial({
            color: 0x0000ff
        });
        var geometry: THREE.Geometry = new THREE.Geometry();
        geometry.vertices.push(new THREE.Vector3(-2.5, 0, 0));
        geometry.vertices.push(new THREE.Vector3(0, 2.5, 0));
        geometry.vertices.push(new THREE.Vector3(2.5, 0, 0));
        var mesh = new THREE.Line(geometry, material);
        this.scene.add(mesh);
    }

    private Create3dText() {
        var loader = new THREE.FontLoader();
        loader.load('https://raw.githubusercontent.com/rollup/three-jsnext/master/examples/fonts/helvetiker_regular.typeface.json', (font) => {
            var geometry: THREE.Geometry = new THREE.TextGeometry('O B E Y', {
                font: font,
                size: 0.5,
                height: 0.125,
                curveSegments: 1,
                bevelEnabled: true,
                bevelThickness: 0.05,
                bevelSize: 0.03,
                bevelSegments: 1
            });
            var material: THREE.Material = new THREE.MeshPhongMaterial({
                color: 0x2eb6ff,
                specular: 0xffffff
            });
            var mesh: THREE.Mesh = new THREE.Mesh(geometry, material);
            mesh.position.set(-1.1, -3, 0);
            this.scene.add(mesh);
        });
    }

    private AddSecondaryPointLight() {
        var light: THREE.PointLight = new THREE.PointLight(0x450a00);
        light.intensity = 2;
        light.position.set(0, 2, -3);
        this.scene.add(light);
    }

    private AddPrimaryPointLight() {
        var light: THREE.PointLight = new THREE.PointLight(0xff00ff);
        light.intensity = 1;
        light.position.set(0, 2, 3);
        this.scene.add(light);
    }

    private AddAmbientLight() {
        var light = new THREE.AmbientLight(0x000055);
        this.scene.add(light);
    }

    private AddDirectionalLight() {
        var light: THREE.DirectionalLight = new THREE.DirectionalLight(0xffff00, 2);
        light.castShadow = true;
        light.shadow.mapSize.width = 2048;
        light.shadow.mapSize.height = 2024;
        light.position.set(7, 20, 3);
        light.lookAt(0, 0, 0);
        this.scene.add(light);
    }
}
